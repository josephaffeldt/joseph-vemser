package br.com.dbccompany.Lotr.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ITEM")
@SequenceGenerator( allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")

public class ItemEntity {

	@Id
	@GeneratedValue ( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE )
	
	@Column(name="ID_ITEM", nullable = false)
	private Integer idItem;
	private String descricao;

	@ManyToMany( mappedBy = "itens" )
	private List<InventarioXItem> inventariosXItem = new ArrayList<>();
	
	
	public List<InventarioXItem> getInventariosXItem() {
		return inventariosXItem;
	}
	public void setInventariosXItem(List<InventarioXItem> inventariosXItem) {
		this.inventariosXItem = inventariosXItem;
	}

	public Integer getIdItem() {
		return idItem;
	}
	public void setIdItem(Integer idItem) {
		this.idItem = idItem;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
