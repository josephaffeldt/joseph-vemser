package br.com.dbccompany.Lotr;

import org.hibernate.Transaction;
import org.hibernate.Session;

import br.com.dbccompany.Lotr.Entity.HibernateUtil;
import br.com.dbccompany.Lotr.Entity.PersonagemEntity;

public class Main {

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			PersonagemEntity pensonagem = new PersonagemEntity();
			pensonagem.setNome("Qualquer");
			
			session.save(pensonagem);
			
			transaction.commit();			
		}catch(Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);		
		}finally {
			System.exit(0);
		}
	}
	
}
