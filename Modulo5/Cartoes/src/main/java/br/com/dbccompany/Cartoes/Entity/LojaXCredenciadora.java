package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name="LOJA_CREDENCIADORA" )
public class LojaXCredenciadora {

	@Id
	@SequenceGenerator( allocationSize=1, name="SEQ_LOJA_X_CREDENCIADORA", sequenceName="SEQ_LOJA_X_CREDENCIADORA" )
	@GeneratedValue ( generator = "SEQ_LOJA_X_CREDENCIADORA", strategy = GenerationType.SEQUENCE )
	private Integer id;
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name = "id_loja",
		joinColumns = {
			@JoinColumn( name = "id_loja_x_credenciadora")},
		inverseJoinColumns = { 
			@JoinColumn( name = "id_loja" )})
	private List<LojaEntity> lojas = new ArrayList<>();
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name = "id_credenciadora",
		joinColumns = {
			@JoinColumn( name = "id_loja_x_credenciadora")},
		inverseJoinColumns = { 
			@JoinColumn( name = "id_credenciadora" )})
	private List<CredenciadoraEntity> credenciadoras = new ArrayList<>();
	
	@Column( name="TAXA", nullable=false )
	private Double taxa;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public List<LojaEntity> getLojas() {
		return lojas;
	}
	public void setLojas(List<LojaEntity> lojas) {
		this.lojas = lojas;
	}

	public List<CredenciadoraEntity> getCredenciadoras() {
		return credenciadoras;
	}
	public void setCredenciadoras(List<CredenciadoraEntity> credenciadoras) {
		this.credenciadoras = credenciadoras;
	}

	public Double getTaxa() {
		return taxa;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
}
