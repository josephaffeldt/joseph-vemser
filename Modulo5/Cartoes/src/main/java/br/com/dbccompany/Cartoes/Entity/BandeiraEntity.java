package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name="BANDEIRA" )
public class BandeiraEntity {

	@Id
	@SequenceGenerator ( allocationSize=1, name="SEQ_BANDEIRA", sequenceName="SEQ_BANDEIRA" )
	@GeneratedValue( generator="SEQ_BANDEIRA", strategy=GenerationType.SEQUENCE )
	
	@Column( name="ID_BANDEIRA", nullable=false )
	private Integer idBandeira;
	
	@Column( name="NOME", nullable=false )
	private String nome;
	
	@Column( name="TAXA", nullable=false )
	private Double taxa;
	
	public Double getTaxa() {
		return taxa;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	
	public Integer getIdBandeira() {
		return idBandeira;
	}
	public void setIdBandeira(Integer idBandeira) {
		this.idBandeira = idBandeira;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
