package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LANCAMENTO")
public class LancamentoEntity {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "SEQ_LANCAMENTO", sequenceName = "SEQ_LANCAMENTO")
	@GeneratedValue(generator = "SEQ_LANCAMENTO", strategy = GenerationType.SEQUENCE)

	@Column(name = "ID_LANCAMENTO", nullable = false)
	private Integer idLancamento;

	@ManyToOne( cascade = CascadeType.ALL ) 
	private CartaoEntity cartao;
	  
	@ManyToOne( cascade = CascadeType.ALL )	 
	private LojaEntity loja;
	 
	@ManyToOne( cascade = CascadeType.ALL )
	private EmissorEntity emissor;

	@Column(name = "DESCRICAO", nullable = false)
	private String descricao;

	@Column(name = "VALOR", nullable = false)
	private Double valor;

	@Column(name = "DATA_COMPRA", nullable = false)
	private Date dataCompra;

	
	public Integer getIdLancamento() {
		return idLancamento;
	}
	public void setIdLancamento(Integer idLancamento) {
		this.idLancamento = idLancamento;
	}

	public CartaoEntity getCartao() {
		return cartao;
	}
	public void setCartao(CartaoEntity cartao) {
		this.cartao = cartao;
	}

	public LojaEntity getLoja() {
		return loja;
	}
	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}
	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}
	
	

}
