package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name="EMISSOR" )
@SequenceGenerator ( allocationSize=1, name="SEQ_EMISSOR", sequenceName="SEQ_EMISSOR" )
public class EmissorEntity {

	@Id
	@GeneratedValue( generator="SEQ_EMISSOR", strategy=GenerationType.SEQUENCE )
	
	@Column( name="ID_EMISSOR", nullable=false )
	private Integer idEmissor;
	
	@Column( name="NOME", nullable=false )
	private String nome;
	
	@Column( name="TAXA", nullable=false )
	private Double taxa;

	public Integer getIdEmissor() {
		return idEmissor;
	}
	public void setIdEmissor(Integer idEmissor) {
		this.idEmissor = idEmissor;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getTaxa() {
		return taxa;
	}
	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
	
}
