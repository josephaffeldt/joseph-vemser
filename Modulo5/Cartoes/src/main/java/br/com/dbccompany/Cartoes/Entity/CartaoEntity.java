package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name="CARTAO" )
public class CartaoEntity {
	
	@Id
	@SequenceGenerator ( allocationSize=1, name="SEQ_CARTAO", sequenceName="SEQ_CARTAO" )
	@GeneratedValue( generator="SEQ_CARTAO", strategy=GenerationType.SEQUENCE )
	
	@Column( name="ID_CARTAO", nullable=false )
	private Integer idCartao;
	
	@Column( name="CHIP", nullable=false )
	private String chip;
	
	@ManyToOne( cascade = CascadeType.ALL )
	private ClienteEntity cliente;
	
	@ManyToOne( cascade = CascadeType.ALL )
	private BandeiraEntity bandeira;
	 
	@ManyToOne( cascade = CascadeType.ALL )
	private EmissorEntity emissor;
	
	@Column( name="VENCIMENTO", nullable=false )
	private Date vencimento;

	public Integer getIdCartao() {
		return idCartao;
	}
	public void setIdCartao(Integer idCartao) {
		this.idCartao = idCartao;
	}

	public String getChip() {
		return chip;
	}
	public void setChip(String chip) {
		this.chip = chip;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}
	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}
	public BandeiraEntity getBandeira() {
		return bandeira;
	}
	public void setBandeira(BandeiraEntity bandeira) {
		this.bandeira = bandeira;
	}
	public EmissorEntity getEmissor() {
		return emissor;
	}
	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}
	public Date getVencimento() {
		return vencimento;
	}
	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
}
