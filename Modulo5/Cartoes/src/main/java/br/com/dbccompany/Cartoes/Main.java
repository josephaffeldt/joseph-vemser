package br.com.dbccompany.Cartoes;

import org.hibernate.Transaction;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.metamodel.ListAttribute;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadoraEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.LojaXCredenciadora;

public class Main {

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		
		List<LojaEntity> lojas = new ArrayList<>();
		List<CredenciadoraEntity> credenciadoras = new ArrayList<>();
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja1 = new LojaEntity();
			loja1.setNome("Loja1");
			lojas.add(loja1);
			session.save(loja1);
			LojaEntity loja2 = new LojaEntity();
			loja2.setNome("Loja2");
			lojas.add(loja2);
			session.save(loja2);
			
			CredenciadoraEntity credenciadora1 = new CredenciadoraEntity();
			credenciadora1.setNome("Credenciadora1");
			credenciadoras.add(credenciadora1);
			session.save(credenciadora1);
			CredenciadoraEntity credenciadora2 = new CredenciadoraEntity();
			credenciadora2.setNome("Credenciadora2");
			credenciadoras.add(credenciadora2);
			session.save(credenciadora2);
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Cliente");
			session.save(cliente);
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("Bandeira");
			bandeira.setTaxa(2.3);
			session.save(bandeira);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Emissor");
			emissor.setTaxa(1.2);
			session.save(emissor);
			
			LojaXCredenciadora loxcre = new LojaXCredenciadora();
			loxcre.setLojas(lojas);
			loxcre.setCredenciadoras(credenciadoras);
			loxcre.setTaxa(3.0);
			session.save(loxcre);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setChip("$#213");
			cartao.setVencimento(new Date(2025,10,02));
			cartao.setBandeira(bandeira);
			cartao.setCliente(cliente);
			cartao.setEmissor(emissor); 
			session.save(cartao);
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setValor(11.0);
			lancamento.setDataCompra(new Date(2020,20,02));
			lancamento.setDescricao("Mercadito Do Bem");
			lancamento.setCartao(cartao);
			lancamento.setEmissor(emissor);
			lancamento.setLoja(loja1);
			session.save(lancamento); 	
			
			
			transaction.commit();
			
			
			Query query1 = session.createQuery("SELECT nome FROM CredenciadoraEntity WHERE idCredenciadora = 1");
			System.out.println(query1.list());
			
			Query query2 = session.createQuery("SELECT nome FROM BandeiraEntity WHERE idBandeira = 1");
			System.out.println(query2.list());
			
			Query query3 = session.createQuery("SELECT taxa FROM EmissorEntity WHERE idEmissor = 1");
			System.out.println(query3.list());
			
			
		}catch(Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);		
		}finally {
			System.exit(0);
		}
	}
}
