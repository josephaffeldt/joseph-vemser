package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name="CREDENCIADORA" )
public class CredenciadoraEntity {

	@Id
	@SequenceGenerator ( allocationSize=1, name="SEQ_CREDENCIADORA", sequenceName="SEQ_CREDENCIADORA" )
	@GeneratedValue( generator="SEQ_CREDENCIADORA", strategy=GenerationType.SEQUENCE )
	
	@Column( name="ID_CREDENCIADORA", nullable=false )
	private Integer idCredenciadora;
	
	@Column( name="NOME", nullable=false )
	private String nome;
	
	@ManyToMany( mappedBy="credenciadoras" )
	private List<LojaXCredenciadora> lojaXCredenciadora = new ArrayList<>();

	public Integer getIdCredenciadora() {
		return idCredenciadora;
	}
	public void setIdCredenciadora(Integer idCredenciadora) {
		this.idCredenciadora = idCredenciadora;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<LojaXCredenciadora> getLojaXCredenciadora() {
		return lojaXCredenciadora;
	}
	public void setLojaXCredenciadora(List<LojaXCredenciadora> lojaXCredenciadora) {
		this.lojaXCredenciadora = lojaXCredenciadora;
	}
	
	
}
