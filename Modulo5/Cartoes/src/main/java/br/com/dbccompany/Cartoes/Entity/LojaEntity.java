package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name="LOJA" )
public class LojaEntity {

	@Id
	@SequenceGenerator ( allocationSize=1, name="SEQ_LOJA", sequenceName="SEQ_LOJA" )
	@GeneratedValue( generator="SEQ_LOJA", strategy=GenerationType.SEQUENCE )
	
	@Column( name="ID_LOJA", nullable=false )
	private Integer idLoja;
	
	@Column( name="NOME", nullable=false )
	private String nome;

	@ManyToMany( mappedBy="lojas" )
	@Column( name="LOJA_X_CREDENCIADORA", nullable=false )
	private List<LojaXCredenciadora> lojaXCredenciadora = new ArrayList<>();

	public Integer getIdLoja() {
		return idLoja;
	}

	public void setIdLoja(Integer idLoja) {
		this.idLoja = idLoja;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<LojaXCredenciadora> getLojaXCredenciadora() {
		return lojaXCredenciadora;
	}

	public void setLojaXCredenciadora(List<LojaXCredenciadora> lojaXCredenciadora) {
		this.lojaXCredenciadora = lojaXCredenciadora;
	}
	
}
