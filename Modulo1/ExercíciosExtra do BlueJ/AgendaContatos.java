import java.util.*;

public class AgendaContatos{
    
    private HashMap< String, String > listaTelefonica;
    
    public AgendaContatos(){
        listaTelefonica = new LinkedHashMap<>();
    }
    
    public HashMap<String, String> getListaTelefonica(){
        return this.listaTelefonica;
    }
    
    public void adicionar( String nome, String numero ){
        listaTelefonica.put(nome, numero);
    }
    
    public String consultar( String nome ){
        return listaTelefonica.get(nome);
    }
    
    public String buscaPeloTel( String numero ){
        /* Correçao do Marcos;
        for( HashMap.Entry<String, String> par : listaTelefonica.entrySet()){
            if( par.getValue().equal(telefone) ){
                return par.getKey;
            }
        }
        */
        
        for ( String nome : listaTelefonica.keySet()){
            if( listaTelefonica.get(nome).equals(numero) ) {
                return nome;            
            }
        }
        
        return null;
    }
   
    public String csv(){
        StringBuilder builder = new StringBuilder();
        
        String separador = System.lineSeparator();
        
        for( HashMap.Entry<String, String > par : listaTelefonica.entrySet() ){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format( "%s,%s%s", chave, valor, separador );
            builder.append(contato);
        }
        
        return builder.toString();
    }
    
}