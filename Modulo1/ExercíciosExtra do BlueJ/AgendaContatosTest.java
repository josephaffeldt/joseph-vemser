import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class AgendaContatosTest{
    @Test 
    public void adicionouContatoNaLista(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        agenda.adicionar("Joseph", "666666");
        
        assertEquals("555555", agenda.getListaTelefonica().get("Marcos"));
        assertEquals("444444", agenda.getListaTelefonica().get("Mithrandir"));
        assertEquals("666666", agenda.getListaTelefonica().get("Joseph"));
    }
    
    @Test 
    public void buscarTelefonePorNome(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        agenda.adicionar("Joseph", "666666");
        
        assertEquals("555555", agenda.consultar("Marcos"));
        assertEquals("444444", agenda.consultar("Mithrandir"));
        assertEquals("666666", agenda.consultar("Joseph"));
    }
    
    @Test 
    public void buscarNomePorTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionar("Marcos", "555555");
        agenda.adicionar("Mithrandir", "444444");
        agenda.adicionar("Joseph", "666666");
        
        assertEquals("Marcos", agenda.buscaPeloTel("555555"));
        assertEquals("Mithrandir", agenda.buscaPeloTel("444444"));
        assertEquals("Joseph", agenda.buscaPeloTel("666666"));
    }
    
    @Test
    public void adicionarContatoEGerarCSV() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Marcos","123123123");
        agenda.adicionar("DBC","555555");
        String separador = System.lineSeparator();
        String esperado = String.format("Marcos,123123123%sDBC,555555%s", separador, separador);
        assertEquals(esperado, agenda.csv());
    }
}
