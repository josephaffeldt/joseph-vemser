public class TradutorParaIngles implements Tradutor{
    
    public String traduzir( String textoEmPortugues ){
        switch(textoEmPortugues){
            case "Sim":
                return "Yes";
            case "Nao":
                return "No";                
            case "Obrigado":
            case "Obrigada":
                return "Thank you";
                
            default :
                return null;
        }
    }
}
