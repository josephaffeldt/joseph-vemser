public class TradutorParaEspanhol implements Tradutor{
    
    public String traduzir( String textoEmPortugues ){
        switch(textoEmPortugues){
            case "Si":
                return "Yes";
            case "Nao":
                return "No";    
            default :
                return null;
        }
    }
}