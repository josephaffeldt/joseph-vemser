import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class PaginadorInventarioTest{    
    @Test
    public void adicionarZeroItensETestarPaginas(){
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario (inventario);
               
        paginador.pular(0);
        
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void adicionarCincoItensETestarPaginas(){
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario (inventario);
                
        Item espada = new Item(1, "Espada");        
        Item faca = new Item(3, "Faca");
        Item shild = new Item(1, "Shild");
        Item flechas = new Item(5, "Flechas");
        Item livro = new Item(3, "Livro");
        
        inventario.adicionar(espada);        
        inventario.adicionar(faca);
        inventario.adicionar(shild);        
        inventario.adicionar(flechas);
        inventario.adicionar(livro);
        
        paginador.pular(0);        
        paginador.limitar(0);
                
        assertEquals(espada, inventario.obter(0));
        assertEquals(faca, inventario.obter(1));
        
        paginador.pular(2);
        paginador.limitar(2);
        
        assertEquals(shild, inventario.obter(2));
        assertEquals(flechas, inventario.obter(3));
    }    
    
    @Test
    public void verificarQuantidadeDePosicoes(){
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario (inventario);
               
        Item espada = new Item(1, "Espada");        
        Item faca = new Item(3, "Faca");
        Item shild = new Item(1, "Shild");
        Item flechas = new Item(5, "Flechas");
        Item livro = new Item(3, "Livro");
        
        inventario.adicionar(espada);        
        inventario.adicionar(faca);
        inventario.adicionar(shild);        
        inventario.adicionar(flechas);
        inventario.adicionar(livro);
        
        paginador.pular(2);
        
        assertEquals(2, paginador.limitar(2).size());
    }
}
