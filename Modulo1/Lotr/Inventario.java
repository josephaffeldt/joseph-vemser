import java.util.*;

 public class Inventario{
    private ArrayList<Item> inventario;
    //private int quantidade;
    //private int count = 0;
        
    /*public Inventario( int quantidade ){;
        this.quantidade = quantidade;
    }
    */
    
    public Inventario(){
        inventario = new ArrayList<Item>();
        //this(99);        
        //inventario = new Item[ 99 ];
        //quantidade = 99;
    }
    
    /*public int getQuantidade(){
        return this.quantidade;
    }
    */
    
    public ArrayList<Item> getItensDoInventario(){
        return this.inventario;
    }
    
    public void adicionar( Item item ){
        this.inventario.add(item);
        //inventario[getCount()] = item;
        //setCount();        
    }
    
    /*Count para validar 
    public int getCount(){
        return this.count;
    }
    
    public void setCount(){
        this.count++;
    }
    */   
    
    public Item obter( int posicao ){
        if ( posicao >= this.inventario.size() || posicao < 0 ){
            return null;
        }
        return this.inventario.get(posicao);
    }
    
    public void remover( Item item ){
        this.inventario.remove(item);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        String s = "";
        int i;
        for( i = 0; i < this.inventario.size(); i++ ){
            Item item = this.inventario.get(i);
            if ( item != null ){
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }            
        }
        return ( descricoes.length() > 0 ? 
        descricoes.substring(0, (descricoes.length() - 1)) : 
        descricoes.toString() );
    }
    
    public Item getItemMaiorQuantidade(){
        Item it = this.inventario.get(0);
        for ( int i = 1; i < this.inventario.size(); i++ ){
            if ( this.inventario.get(i) != null ){
                if ( this.inventario.get(i).getQuantidade() > it.getQuantidade() ){
                    it = this.inventario.get(i);
                }
            }                           
        }
        return this.inventario.size() > 0 ? it : null;
    }
    
    public Item buscar ( String str ){
        /* For each
         * o tipo de retorno : onde vai buscar
        for( Item itemAtual : this.inventario ){
            boolean encontrei = itemAtual.getDescricao().equals(str);   
            if( encontrei ){ return itemAtual; }
        }*/
        
        for ( int i = 0; i < this.inventario.size(); i++ ){
            if ( this.obter(i).getDescricao().equals(str) ){
                return this.obter(i);
            }
        }        
        return null;
    } 
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> invertida = new ArrayList<>();
        
        for ( int i = (this.inventario.size()-1); i >= 0; i-- ){
                invertida.add(this.obter(i));
        }
        
        return invertida;
    }
    
    public void setInventario(ArrayList<Item> inventario){
        this.inventario = inventario;
    }
    
    public void ordenarItens(){  
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens( TipoOrdenacao tipoOrd ){
        Item aux = obter(0);
        
        for ( int i = 0; i < this.inventario.size()-1; i++ ){
                for( int j = i+1; j < this.inventario.size(); j++ ){
                    Item atual = this.obter(i);
                    Item proximo = this.obter(j);
                
                    boolean deveTrocar = tipoOrd == TipoOrdenacao.ASC ?
                    atual.getQuantidade() > proximo.getQuantidade() :
                    atual.getQuantidade() < proximo.getQuantidade();
                
                    if( deveTrocar ){
                        aux = atual;
                        this.getItensDoInventario().set(i, proximo);
                        this.getItensDoInventario().set(j, aux);
                    }
                }
            }
    }
    
    public Inventario unir( Inventario invent ){
        Inventario novoInventario = new Inventario ();
        
        for (int i = 0; i < this.getItensDoInventario().size(); i++){
            novoInventario.adicionar(this.obter(i));            
        }
        
        int size = invent.getItensDoInventario().size();
        for(int i = 0; i < size; i++ ){
            novoInventario.adicionar(invent.obter(i));
        }
        return novoInventario;
    }
    
    /*
    public Inventario diferenciar( Inventario invent ){
        Inventario novoInventario = new Inventario ();
        
        for (int i = 0; i < this.getItensDoInventario().size(); i++){
            novoInventario.adicionar(this.obter(i));            
        }
        
        int size = invent.getItensDoInventario().size();
        for(int i = 0; i < size; i++ ){
            novoInventario.adicionar(invent.obter(i));
        }
        return novoInventario;
    }
    */
    
    
}