import java.util.*;

public class EstatisticasInventario{
    
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
        
    public Inventario getInventario(){
        return this.inventario;
    }
    
    public double calcularMedia(){
        double soma = 0;
        
        if ( this.inventario.getItensDoInventario().isEmpty() ){
            return Double.NaN;
        }
        
        for ( int i = 0; i < inventario.getItensDoInventario().size(); i++ ){
            soma = soma + inventario.obter(i).getQuantidade();
        }
        
        return ( soma / inventario.getItensDoInventario().size() );
    }
    
    public double calcularMediana(){
        int mediana = 0;
        
        ArrayList <Integer> aux = new ArrayList<Integer>();
        
        for ( int i = 0; i < inventario.getItensDoInventario().size()-1; i++ ){
            if (inventario.obter(i).getQuantidade() > inventario.obter(i+1).getQuantidade()){
                aux.add( inventario.obter(i+1).getQuantidade() );
            }
            else{
                aux.add( inventario.obter(i).getQuantidade());
            }
        }
        
        if( (aux.size() % 2) != 0 ){
            mediana = aux.get( (int) aux.size() / 2 );
        }
        else {
            mediana = ( aux.get( (int) aux.size() / 2) + aux.get( (int) (aux.size() / 2 ) + 1) ) / 2;
        }               
        return mediana;
    }
    
    public int qtdItensAcimaDaMedia(){
        int count = 0;
        for ( int i = 0; i < inventario.getItensDoInventario().size(); i++ ){
            if ( inventario.obter(i).getQuantidade() > this.calcularMedia()){
                count ++;
            }
        }
        return count;
    } 
    
}
