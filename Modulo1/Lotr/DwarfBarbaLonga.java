public class DwarfBarbaLonga extends Dwarf {

    public Sorteador sorteador;

    public DwarfBarbaLonga( String nome ){
        super(nome);
        sorteador = new DadoD6();
    }

    public DwarfBarbaLonga( String nome, Sorteador sorteador ){
        super(nome);
        sorteador = sorteador;
    }

    @Override
    public void danoTomado(){
        boolean devePerderVida = sorteador.sortear() > 2;
        if( devePerderVida ){                
            super.danoTomado();
        }         
    }
}
