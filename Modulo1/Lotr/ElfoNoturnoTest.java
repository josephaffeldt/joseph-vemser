import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoNoturnoTest{
    @Test    
    public void atiraFlechaDiminuirFlechAumentarXpEm3vezes(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Legolas");        
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        novoElfoNoturno.atirarFlecha(novoDwarf);
        
        //testes para verificar quantas flechas e quanto de exp tem.
        assertEquals(3, novoElfoNoturno.getExperiencia());
        assertEquals(1, novoElfoNoturno.getQtdFlecha());
    }
    
    @Test    
    public void atiraFlechaDiminuiDanoEm15(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Legolas");        
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        novoElfoNoturno.atirarFlecha(novoDwarf);
        
        assertEquals(85.0, novoElfoNoturno.getVida(), 1e-9);
        assertEquals(Status.SOFREU_DANO, novoElfoNoturno.getStatus());
    }
    
    @Test    
    public void morreCom7Ataques(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Legolas"); 
        novoElfoNoturno.getFlecha().setQuantidade(1000);
        
        for (int i = 0; i < 7; i++){
            novoElfoNoturno.danoTomado();
        }
        
        assertEquals(0, novoElfoNoturno.getVida(), 1e-9);
        assertEquals(Status.MORTO, novoElfoNoturno.getStatus());
    }
}
