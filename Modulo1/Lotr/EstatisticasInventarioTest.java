import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest{
    @Test
    public void calculaAMediaDoInventario(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        Item faca = new Item(3, "Faca");
        Item arco = new Item(1, "Arco");
        Item flechas = new Item(5, "Flechas");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);
        inventario.adicionar(faca);
        inventario.adicionar(arco);
        inventario.adicionar(flechas);
        
        EstatisticasInventario estInventario = new EstatisticasInventario (inventario);
        
        assertEquals(2.2, estInventario.calcularMedia(), 1e-9 );    
    }
    
    @Test
    public void calculaAMedianaPar(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        Item faca = new Item(3, "Faca");
        Item arco = new Item(1, "Arco");
        Item flechas = new Item(5, "Flechas");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);
        inventario.adicionar(faca);
        inventario.adicionar(arco);
        inventario.adicionar(flechas);
        
        EstatisticasInventario estInventario = new EstatisticasInventario (inventario);
        
        assertEquals(1, estInventario.calcularMediana(), 1e-9);    
    }
    
    @Test
    public void calculaAMedianaImpar(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        Item faca = new Item(3, "Faca");
        Item arco = new Item(1, "Arco");
        Item flechas = new Item(5, "Flechas");
        Item livro = new Item(3, "Livro");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);
        inventario.adicionar(faca);
        inventario.adicionar(arco);
        inventario.adicionar(flechas);
        inventario.adicionar(livro);
        
        EstatisticasInventario estInventario = new EstatisticasInventario (inventario);
        
        assertEquals(1, estInventario.calcularMediana(), 1e-9);    
    }
    
    @Test
    public void quantidadeDeItensAcimaDaMedia(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        Item faca = new Item(3, "Faca");
        Item arco = new Item(1, "Arco");
        Item flechas = new Item(5, "Flechas");
        Item livro = new Item(3, "Livro");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);
        inventario.adicionar(faca);
        inventario.adicionar(arco);
        inventario.adicionar(flechas);
        inventario.adicionar(livro);
        
        EstatisticasInventario estInventario = new EstatisticasInventario (inventario);
        
        assertEquals(3, estInventario.qtdItensAcimaDaMedia());    
    }
}
