public class Elfo extends Personagem{
    
    private int indiceFlecha;
    private static int quantidadeElfosInstanciados = 0;
    //private Item flecha = new Item(2,"Flecha");
    //private Item arco = new Item(1,"Arco");
        
    //Usado para separar escopos, neste bloco e inicializado as variaveis
    {
        indiceFlecha = 0; 
    }
    
    public Elfo( String nome ){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(2,"Flecha"));
        this.inventario.adicionar(new Item(1,"Arco"));
        Elfo.quantidadeElfosInstanciados ++;
    }    
    
    protected void finalize() throws Throwable{
        Elfo.quantidadeElfosInstanciados--;
    }
    
    public static int getQtdElfos(){
        return Elfo.quantidadeElfosInstanciados;
    }
    
    public int getQuantidadeElfosInstanciados(){
        return this.quantidadeElfosInstanciados;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }    
    
    public boolean podeAtirarFlecha(){
        return this.getQtdFlecha() > 0;
    }
    
    public void atirarFlecha( Dwarf dwarf ){
        if ( podeAtirarFlecha() && ( dwarf.getVida() > 0 )){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentaXp();
            dwarf.danoTomado();
            this.danoTomado();
        }        
    } 
}
