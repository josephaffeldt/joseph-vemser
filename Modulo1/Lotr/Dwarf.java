public class Dwarf extends Personagem{
   private boolean equipado = false;
   
   public Dwarf( String nome ){
       super(nome);
       this.vida = 110.0;
       this.qtdDano = 10.0;
       Item escudo = new Item (1, "Escudo");
       this.ganharItem(escudo);
   }     
     
   public void equipaOShild(){
       this.qtdDano = 5.0;
   }  
}
