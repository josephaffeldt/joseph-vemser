import java.util.*;

public class AtaqueAlternado implements EstrategiasDeAtaque{

    public ArrayList<Elfo> colocaNaFrenteElfoVerde ( ArrayList<Elfo> atacantes ){

        Collections.sort(atacantes, new Comparator<Elfo>(){
                public int compare( Elfo elfoAtual, Elfo elfoProximo ){
                    boolean mesmoTipo = elfoAtual.getClass() == elfoProximo.getClass();

                    return mesmoTipo ? -1 : 0;
                }            
            } ); 
        return atacantes;
    }

    public ArrayList<Elfo> getOrdemDeAtaque( ArrayList<Elfo> atacantes ){
        atacantes = this.colocaNaFrenteElfoVerde(atacantes);

        int count = 0;
        boolean flag = false;

        for( int i = 0; i < atacantes.size()-1; i++){
            if ( !atacantes.get(i).equals(atacantes.get(i+1)) ){
                count++;
            }
        }
        
        for (int i = count; i < atacantes.size(); i++){
            atacantes.remove(i);
        }
        
        if ( (atacantes.size() % 2) == 0 ){
            flag = true;
        }
        
        return atacantes;
    }
}
