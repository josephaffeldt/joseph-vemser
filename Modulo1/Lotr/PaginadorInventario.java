import java.util.*;

public class PaginadorInventario{
    
    private Inventario inventario;
    private int indice;
    
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public void pular ( int pular ){
        this.indice = pular > 0 ? pular : 0;
    }
    
    public int getIndice(){
        return this.indice;
    }
            
    public ArrayList<Item> limitar( int limitar ){
        ArrayList<Item> listaItens = new ArrayList<Item>();
        
        int fim = getIndice() + limitar;
        
        if (getIndice() < inventario.getItensDoInventario().size()){
            for ( int i = getIndice(); ( i < fim && i < inventario.getItensDoInventario().size() ); i++ ){
                listaItens.add(inventario.obter(i));
            }
        }
        
        return listaItens;
    }
  
}
