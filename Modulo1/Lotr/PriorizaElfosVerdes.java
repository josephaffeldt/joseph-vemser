import java.util.*;

public class PriorizaElfosVerdes implements EstrategiasDeAtaque{

    public ArrayList<Elfo> colocaNaFrenteElfoVerde ( ArrayList<Elfo> atacantes ){
        
        Collections.sort(atacantes, new Comparator<Elfo>(){
            public int compare( Elfo elfoAtual, Elfo elfoProximo ){
                boolean mesmoTipo = elfoAtual.getClass() == elfoProximo.getClass();
                
                if ( mesmoTipo ){
                    return 0;
                }
                
                return elfoAtual instanceof ElfoVerde && elfoProximo instanceof ElfoNoturno ? -1 : 1;
            }            
        } ); 
        return atacantes;
    }

    public ArrayList<Elfo> getOrdemDeAtaque( ArrayList<Elfo> atacantes ){
        return this.colocaNaFrenteElfoVerde(atacantes);
    }
}
