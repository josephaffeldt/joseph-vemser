import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest{
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario();        
        Item espada = new Item(1, "Espada");        
        inventario.adicionar(espada);        
        assertEquals(espada, inventario.getItensDoInventario().get(0));        
        //assertEquals(espada, inventario.obter(0));
    }
    
    @Test
    public void adicionarDoisItem(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);
        
        assertEquals(espada, inventario.getItensDoInventario().get(0));        
        //assertEquals(espada, inventario.obter(0));
        
        assertEquals(shild, inventario.getItensDoInventario().get(1));        
        //assertEquals(shild, inventario.obter(1));
    }
    
    @Test
    public void obterItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada"); 
        inventario.adicionar(espada);        
        assertEquals(espada, inventario.obter(0));     
    }
    
    @Test
    public void removerSegundoItem(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild); 
        
        inventario.remover(espada);
        
        assertEquals(1, inventario.getItensDoInventario().size());     
    }
    
    @Test
    public void removePrimeiroItem(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        
        inventario.adicionar(espada);
        
        inventario.remover(espada);
        
        assertEquals(0, inventario.getItensDoInventario().size());     
    }
    
    @Test
    public void buscaItemPorDescricao(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild); 
                
        assertEquals(espada, inventario.buscar("Espada"));     
    }
    
    @Test
    public void buscaNenhumItemPorDescricao(){
        Inventario inventario = new Inventario();               
        
        assertEquals("", inventario.getDescricoesItens());     
    }
    
    @Test
    public void buscaItemPorMesmaDescricao(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Espada");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild); 
                
        assertEquals(espada, inventario.buscar("Espada"));   
    }
    
    @Test
    public void getItemMaiorQuantidade(){
        Inventario inventario = new Inventario(); 
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        Item flecha = new Item(3, "Flecha");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);      
        inventario.adicionar(flecha); 
        
        assertEquals(flecha, inventario.getItemMaiorQuantidade());     
    }
    
    @Test
    public void getItemMaiorQuantidadeIguais(){
        Inventario inventario = new Inventario(); 
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        Item flecha = new Item(1, "Flecha");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);      
        inventario.adicionar(flecha); 
        
        assertEquals(espada, inventario.getItemMaiorQuantidade());     
    }    
    
    @Test
    public void inverteArrayOriginal(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);
        
        ArrayList<Item> it = new ArrayList<>();
        
        it.add(shild);
        it.add(espada);
                
        assertEquals(it, inventario.inverter()); 
        
        /*Feito pelo Marcos
        
        Item espada = new Item(1, "Espada");
        Item shild = new Item(1, "Shild");
        
        inventario.adicionar(espada);
        inventario.adicionar(shild);
        
        assertEquals(shild, inventario.inverter().get(0)); 
        assertEquals(espada, inventario.inverter().get(i));
        assertEquals(2, inventario.getItensDoInventario().size());         
        
        */
    }
    
    @Test
    public void inverteArrayVazio(){
        Inventario inventario = new Inventario();
                
        assertTrue(inventario.inverter().isEmpty());     
    }
    
    @Test
    public void inverteArrayComUm(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");        
        inventario.adicionar(espada);
        
        assertEquals(espada, inventario.inverter().get(0));
        assertEquals(1, inventario.getItensDoInventario().size());        
    }
    
    @Test
    public void ordenaListaAscedente(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");        
        Item faca = new Item(3, "Faca");
        Item shild = new Item(1, "Shild");
        Item flechas = new Item(5, "Flechas");
        Item livro = new Item(3, "Livro");
        
        inventario.adicionar(espada);        
        inventario.adicionar(faca);
        inventario.adicionar(shild);        
        inventario.adicionar(flechas);
        inventario.adicionar(livro);
        
        inventario.ordenarItens();
        
        assertEquals(espada, inventario.obter(0));
        assertEquals(shild, inventario.obter(1));
        assertEquals(faca, inventario.obter(2));
        assertEquals(livro, inventario.obter(3));
        assertEquals(flechas, inventario.obter(4));                
    }
    
    @Test
    public void ordenaListaDescedente(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");        
        Item faca = new Item(3, "Faca");
        Item shild = new Item(1, "Shild");
        Item flechas = new Item(5, "Flechas");
        Item livro = new Item(3, "Livro");
        
        inventario.adicionar(espada);        
        inventario.adicionar(faca);
        inventario.adicionar(shild);        
        inventario.adicionar(flechas);
        inventario.adicionar(livro);
        
        inventario.ordenarItens(TipoOrdenacao.DESC);
        
        assertEquals(flechas, inventario.obter(0));
        assertEquals(faca, inventario.obter(1));
        assertEquals(livro, inventario.obter(2));
        assertEquals(espada, inventario.obter(3));
        assertEquals(shild, inventario.obter(4));                
    }
    
    @Test
    public void ordenaListaAscedentePassadoPorParametro(){
        Inventario inventario = new Inventario();
        
        Item espada = new Item(1, "Espada");        
        Item faca = new Item(3, "Faca");
        Item shild = new Item(1, "Shild");
        Item flechas = new Item(5, "Flechas");
        Item livro = new Item(3, "Livro");
        
        inventario.adicionar(espada);        
        inventario.adicionar(faca);
        inventario.adicionar(shild);        
        inventario.adicionar(flechas);
        inventario.adicionar(livro);
        
        inventario.ordenarItens(TipoOrdenacao.ASC);
        
        assertEquals(espada, inventario.obter(0));
        assertEquals(shild, inventario.obter(1));
        assertEquals(faca, inventario.obter(2));
        assertEquals(livro, inventario.obter(3));
        assertEquals(flechas, inventario.obter(4));                
    }
}
