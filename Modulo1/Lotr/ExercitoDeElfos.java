import java.util.*;

public class ExercitoDeElfos {

    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfoVerde.class,
                ElfoNoturno.class
            )
        );

    protected ArrayList<Elfo> exercitoDeElfos;    
    private HashMap< Status, ArrayList<Elfo> > porStatus = new HashMap<>();

    public ExercitoDeElfos(){
        this.exercitoDeElfos = new ArrayList<Elfo>();
    }
    
    public ArrayList<Elfo> getExercito(){
        return this.exercitoDeElfos;
    }

    public void alistarElfo( Elfo elfo ){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());

        if( podeAlistar ){
            this.exercitoDeElfos.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get( elfo.getStatus() );

            if ( elfoDoStatus == null){
                elfoDoStatus = new ArrayList<Elfo>();
                porStatus.put(elfo.getStatus(), elfoDoStatus);
            }

            elfoDoStatus.add(elfo);
        }
    }

    public ArrayList<Elfo> buscarElfoPorStatus ( Status status ){
        return this.porStatus.get(status);
    }
}
