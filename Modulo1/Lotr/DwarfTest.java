import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    @Test
    public void dwarfNascemCom110DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        
        //Verifica se o Dwarf tem 
        assertEquals(110.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerdeuVida(){ 
        Dwarf dwarf = new Dwarf("Mulungrid");
        
        /*
        Elfo novoElfo = new Elfo("Legolas");
        quantidade de flechas atiradas
        novoElfo.atirarFlecha(dwarf); 
        */
        
        //Dwarf Sofreu dano
        dwarf.danoTomado();
        
        //Verifica se o Dwarf tem 100 de vida
        assertEquals(100.0, dwarf.getVida(), 1e-9);      
    }
    
    @Test
    public void dwarfMorreuCom11Ataques(){          
        Dwarf dwarf = new Dwarf("Mulungrid");
        
        /* Mata o dwarf
        while(dwarf.getVida()>0){
            dwarf.danoTomado();
        }*/
       
        //Acerta 11 vezes o dwarf;
        for(int i=0; i<11; i++){
            dwarf.danoTomado();
        }    
                        
        //Verifica se o Dwarf morreu
        assertEquals(0, dwarf.getVida(), 1e-9);
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfMorreuCom12Ataques(){         
        Dwarf dwarf = new Dwarf("Mulungrid");
                   
        //Acerta 11 vezes o dwarf;
        for(int i=0; i<12; i++){
            dwarf.danoTomado();
        }         
        
        //Verifica se o Dwarf tem 100 de vida
        assertEquals(0, dwarf.getVida(), 1e-9);      
    }
    
    @Test
    public void dwarfNasceComStatusRecemCriado(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    } 
    
    @Test
    public void dwarfMorreuEMudouStatus(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        
        for(int i=0; i<11; i++){
            dwarf.danoTomado();
        }
        
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeVidaEContinuaVivo(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.danoTomado();
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeVidaEDeveMorrer(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        
        for( int i = 0; i < 11; i++ ) {
            dwarf.danoTomado();
        }
        
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals("Escudo", dwarf.getInventario().buscar("Escudo").getDescricao());
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equipaOShild();
        dwarf.danoTomado();
        assertEquals(105.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.danoTomado();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }
    
}