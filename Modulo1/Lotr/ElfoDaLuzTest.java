import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    @Test    
    public void verificarSeComecouComAEspadaDeGalvorn(){
        ElfoDaLuz galadriel = new ElfoDaLuz("Galadriel");
        
        Item espadaDeGalvor = new Item(1,"Espada de Galvorn");
        
        assertTrue(galadriel.getInventario().obter(2).equals(espadaDeGalvor));        
    }
    
    @Test    
    public void atacouUmaVezUmDwarf(){
        ElfoDaLuz galadriel = new ElfoDaLuz("Galadriel");
        
        galadriel.atacarComEspada( new Dwarf ("Gimli"));
        
        assertEquals(79, galadriel.getVida(), 1e-9);        
    }
    
    @Test    
    public void atacouDuasVezesUmDwarf(){
        ElfoDaLuz galadriel = new ElfoDaLuz("Galadriel");
        
        Dwarf dwarf = new Dwarf ("Gimli");
        
        galadriel.atacarComEspada( dwarf );
        galadriel.atacarComEspada( dwarf );
        
        assertEquals(89, galadriel.getVida(), 1e-9);       
    }
    
    @Test    
    public void verificarSePodePerderAEspadaDeGalvorn(){
        ElfoDaLuz galadriel = new ElfoDaLuz("Galadriel");
        
        Item espadaDeGalvor = new Item(1,"Espada de Galvorn");
        galadriel.perderItem(espadaDeGalvor);
        
        assertTrue(galadriel.getInventario().obter(2).equals(espadaDeGalvor));        
    }
}
