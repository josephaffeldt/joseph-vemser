import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoDeElfosTest{
    @Test
    public void adicionarDoisElfosNoExercito(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        
        Elfo luthien =  new ElfoNoturno("Lúthien");
        Elfo celebron = new ElfoVerde("Celebron");
        
        exercito.alistarElfo(luthien);
        exercito.alistarElfo(celebron);
        
        assertEquals(luthien, exercito.getExercito().get(0));
        assertEquals(celebron, exercito.getExercito().get(1));        
    }
    
    @Test
    public void adicionarElfoInvalidoNoExercito(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        
        Elfo luthien =  new ElfoNoturno("Lúthien");
        Elfo celebron = new ElfoVerde("Celebron");
        Elfo galadriel = new ElfoDaLuz("Galadriel");
        
        exercito.alistarElfo(luthien);
        exercito.alistarElfo(celebron);
        exercito.alistarElfo(galadriel);
        
        assertEquals(luthien, exercito.getExercito().get(0));
        assertEquals(celebron, exercito.getExercito().get(1));
        assertFalse(exercito.getExercito().contains(galadriel));
    }
    
    @Test
    public void buscarListaPorStatus(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        
        Elfo luthien =  new ElfoNoturno("Lúthien");
        Elfo celebron = new ElfoVerde("Celebron");
        
        exercito.alistarElfo(luthien);
        exercito.alistarElfo(celebron);
        
        assertEquals(2, exercito.buscarElfoPorStatus(Status.RECEM_CRIADO).size());
    }
    
    @Test
    public void buscarListaPorStatus2(){
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        
        Elfo luthien =  new ElfoNoturno("Lúthien");
        Elfo celebron = new ElfoVerde("Celebron");
        Elfo galadriel = new ElfoNoturno("Galadriel");
        
        exercito.alistarElfo(luthien);
        exercito.alistarElfo(celebron);        
        exercito.alistarElfo(galadriel);
        
        exercito.getExercito().get(2).atirarFlecha(new Dwarf("Gimli"));
        
        assertEquals(1, exercito.buscarElfoPorStatus(Status.SOFREU_DANO).size());
    }
}
