import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest{
   @Test    
    public void atiraFlechaDiminuirFlechaAumentarXpEm2vezes(){
        ElfoVerde celebron = new ElfoVerde("Celebron");        
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        //quantidade de flechas atiradas
        celebron.atirarFlecha(novoDwarf);
        
        //testes para verificar quantas flechas e quanto de exp tem.
        
        assertEquals(2, celebron.getExperiencia());
        assertEquals(1, celebron.getQtdFlecha());
   }
   
   @Test    
    public void adicionarItemNoInventario(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        
        Item flechaDeVidro = new Item (2, "Flecha de Vidro");        
        
        celebron.ganharItem(flechaDeVidro);
        
        Inventario inventario = celebron.getInventario();
        
        assertEquals(new Item (2,"Flecha"), inventario.obter(0));
        assertEquals(new Item (1, "Arco"), inventario.obter(1));
        assertEquals(flechaDeVidro, inventario.obter(2));
   }
   
   @Test    
    public void elfoVerdeAdicionaItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        
        Item flechaDeVidro = new Item (2, "Flecha de Vidro");        
        
        celebron.ganharItem(flechaDeVidro);
        
        Inventario inventario = celebron.getInventario();
        
        assertEquals(new Item (2, "Flecha"), inventario.obter(0));
        assertEquals(new Item (1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Flecha de Madeira"));
   }
   
   @Test    
    public void adicionar3ItensNoInventario(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        
        Item flecha = new Item (2, "Flecha de Vidro");        
        Item arco = new Item (1, "Arco de Vidro");
        Item espada = new Item (1, "Espada de aço valiriano");
        
        celebron.ganharItem(flecha);
        celebron.ganharItem(arco);
        celebron.ganharItem(espada);
        
        assertEquals(flecha.getDescricao(), celebron.getInventario().obter(2).getDescricao());
        assertEquals(arco.getDescricao(), celebron.getInventario().obter(3).getDescricao());
        assertEquals(espada.getDescricao(), celebron.getInventario().obter(4).getDescricao());
   } 
   
   @Test   
    public void perderItemDoInventarioComDescricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        
        Item flechaDeVidro = new Item (2, "Flecha de Vidro");        
        
        celebron.ganharItem(flechaDeVidro);
        celebron.perderItem(flechaDeVidro);
        
        Inventario inventario = celebron.getInventario();
        
        assertEquals(new Item (2, "Flecha"), inventario.obter(0));
        assertEquals(new Item (1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Flecha de Vidro"));        
   }
   
   @Test    
    public void perderItemDoInventarioComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        
        Item flechaDeVidro = new Item (2, "Flecha de Vidro");        
        
        celebron.perderItem(flechaDeVidro);
        
        Inventario inventario = celebron.getInventario();
        
        assertEquals(new Item (2, "Flecha"), inventario.obter(0));
        assertEquals(new Item (1, "Arco"), inventario.obter(1));    
   }
   
   @Test    
    public void remover3ItensNoInventario(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        
        Item flecha = new Item (2, "Flecha de Vidro");        
        Item arco = new Item (1, "Arco de Vidro");
        Item espada = new Item (1, "Espada de aço valiriano");
        
        celebron.ganharItem(flecha);
        celebron.ganharItem(arco);
        celebron.ganharItem(espada);
        
        assertEquals(flecha.getDescricao(), celebron.getInventario().obter(2).getDescricao());
        assertEquals(arco.getDescricao(), celebron.getInventario().obter(3).getDescricao());
        assertEquals(espada.getDescricao(), celebron.getInventario().obter(4).getDescricao());
        
        celebron.perderItem(flecha);
        celebron.perderItem(arco);
        celebron.perderItem(espada);
        
        assertNull(celebron.getInventario().obter(2));
        assertNull(celebron.getInventario().obter(3));
        assertNull(celebron.getInventario().obter(4));
   }  
}
