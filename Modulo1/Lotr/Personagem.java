public class Personagem{

    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida, qtdDano;
    protected int experiencia, qtdXpPorAtaque;

    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario();
        experiencia = 0;
        qtdDano = 0;
        qtdXpPorAtaque = 1;
    }

    public Personagem( String nome ){
        this.nome = nome;
    }

    public String getNome (){
        return this.nome;
    }

    public void setNome( String nome ){
        this.nome = nome;
    }

    public double getVida(){
        return this.vida;
    }

    public Status getStatus (){
        return this.status;
    }

    public int getExperiencia (){
        return this.experiencia;
    }

    public Inventario getInventario(){
        return this.inventario;
    }

    public void aumentaXp(){
        this.experiencia =  this.experiencia + qtdXpPorAtaque;
    }

    public boolean podeSofrerDano(){       
        if (status == Status.MORTO){
            return false;
        }

        return true;
    }    

    public void ganharItem( Item item ){
        inventario.adicionar(item);
    }

    public void perderItem( Item item ) {       
        inventario.remover(item);
    }

    public void danoTomado(){
        if ( this.podeSofrerDano() && this.qtdDano > 0.0){
            // comparaçao ? verdadeiro : falso;

            this.vida = this.vida >= this.qtdDano ? 
                this.vida - this.qtdDano : 0.0;

            if (this.vida == 0){
                status = Status.MORTO;
            }
            else {
                status = Status.SOFREU_DANO;
            }
        }              
    }
}