import java.util.*;
public class ElfoDaLuz extends Elfo{
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<String>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    private int numAtaques;
    private final double QTD_VIDA_GANHADA = 10;
    
    {
        numAtaques = 0;
    }
    
    public ElfoDaLuz( String nome ){
        super(nome);
        super.ganharItem( new ItemSempreExistente( 1, DESCRICOES_OBRIGATORIAS.get(0) ) );
    }
    
    private boolean devePerderVida(){
        return numAtaques % 2 == 1;
    }
    
    private void ganharVida(){
        this.vida = vida + QTD_VIDA_GANHADA;
    }
        
    public int getNumAtaque(){
        return this.numAtaques;
    }
    
    public void atacarComEspada ( Dwarf dwarf ){
        if( this.getStatus() != Status.MORTO ){
            dwarf.danoTomado();
            this.aumentaXp();
            numAtaques++;
            if( this.devePerderVida() ){
                this.qtdDano = 21.0;
                this.danoTomado();
                this.qtdDano = 0.0;
            }else{
                this.ganharVida();
            }        
        }
    }
    
    @Override
    public void perderItem( Item item ) { 
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if ( possoPerder ){
            super.perderItem(item);
        }
    }
}
