import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test    
    public void atiraFlechaDiminuirFlechAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");        
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        //quantidade de flechas atiradas
        novoElfo.atirarFlecha(novoDwarf);
        
        //testes para verificar quantas flechas e quanto de exp tem.
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
    }
    
    @Test    
    public void atiraFlechaDiminuir2FlechAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas"); 
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        //quantidade de flechas atiradas
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        
        //testes para verificar quantas flechas e quanto de exp tem.
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha()); 
    }
    
    @Test    
    public void atiraFlechaDiminuir3FlechAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas"); 
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        //quantidade de flechas atiradas
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        
        //testes para verificar quantas flechas e quanto de exp tem.
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha()); 
    }  
    
    @Test
    public void elfoNasceComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void naoCriaElfoNaoIncremta(){
        assertEquals(0, Elfo.getQtdElfos() );    
    }
    
    @Test
    public void cria1ElfoContadorUmaVez(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals( 1, Elfo.getQtdElfos() );
    }
    
    @Test
    public void cria2ElfosContadorDuasVezes(){
        Elfo novoElfo = new Elfo("Legolas");
        Elfo elfoVerde = new Elfo("Legolas");
        assertEquals( 2, Elfo.getQtdElfos() );
    }
    
    @Test
    public void cria3ElfosContadorTresVezes(){
        Elfo elfoDaLuz = new ElfoDaLuz("Legolas");
        Elfo elfoVerde = new ElfoVerde("Legolas");
        Elfo elfoNoturno = new ElfoNoturno("Legolas");
        
        assertEquals( 3, Elfo.getQtdElfos() );
    }
}
