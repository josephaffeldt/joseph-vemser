let moedas = (function () {
    //Tudo privado
    function imprimirMoeda(params) {
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow(10, precisao);
            return Math.ceil(numero * fator) / fator;
        }
        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params
        let qtdcasasMilhares = 3
        let StringBuffer = []
        let partedecimal = arredondar(Math.abs(numero) % 1) //formatacao do arredondamento
        let parteInteira = Math.trunc(numero)
        let parteInteiraString = Math.abs(parteInteira).toString()
        let parteInteiraTamanho = parteInteiraString.length
        let c = 1
        while (parteInteiraString > 0) { //é pra fazer o acrecimos dos pontos nas casas de milhares
            if (c % qtdcasasMilhares == 0) {
                StringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
                parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
            } else if (parteInteiraString.length <= qtdcasasMilhares) {
                StringBuffer.push(parteInteiraString)
                parteInteiraString = ''
            }
            c++
        }
        StringBuffer.push(parteInteiraString)

        let decimalString = partedecimal.toString().replace('0.', '').padStart(2, 0);
        const numeroFormatado = `${StringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }

    //Tudo publico, tudo que ta fora do return é privado
    return {
        imprimirBRL: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${numeroFormatado}`,
                colocarNegativo: numeroFormatado => `- ${numeroFormatado}`
            }),
        imprimirGBP: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => `£ ${numeroFormatado}`,
                colocarNegativo: numeroFormatado => `- ${numeroFormatado}`
            }),
        imprimirFR: (numero) =>
            imprimirMoeda({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => ` ${numeroFormatado} €`,
                colocarNegativo: numeroFormatado => `- ${numeroFormatado}`
            }),
    }
})()

//os dois parenteses é para que ela possa ser acessivel
/* console.log(moedas.imprimirGBP(100000));
console.log(moedas.imprimirGBP(4.650)); // "RS 4,66"
console.log(moedas.imprimirGBP(0)) // “R$ 0,00”
console.log(moedas.imprimirGBP(3498.99)) // “R$ 3.498,99”
console.log(moedas.imprimirGBP(-3498.99)) // “-R$ 3.498,99”
console.log(moedas.imprimirGBP(2313477.0135)) // “R$ 2.313.477,02”
console.log("\n"); */

/* console.log(moedas.imprimirGBP(100000));
console.log(moedas.imprimirGBP(4.650)); // "RS 4,66"
console.log(moedas.imprimirGBP(0)) // “R$ 0,00”
console.log(moedas.imprimirGBP(3498.99)) // “R$ 3.498,99”
console.log(moedas.imprimirGBP(-3498.99)) // “-R$ 3.498,99”
console.log(moedas.imprimirGBP(2313477.0135)) // “R$ 2.313.477,02”
console.log("\n"); */

/* console.log(moedas.imprimirFR(100000));
console.log(moedas.imprimirFR(4.650)); // "RS 4,66"
console.log(moedas.imprimirFR(0)) // “R$ 0,00”
console.log(moedas.imprimirFR(3498.99)) // “R$ 3.498,99”
console.log(moedas.imprimirFR(-3498.99)) // “-R$ 3.498,99”
console.log(moedas.imprimirFR(2313477.0135)) // “R$ 2.313.477,02”
console.log("\n");
 */


//Exercício extra achar os bugs
function cardapioIFood(veggie = true, comLactose = false) {
    let cardapio = [
        'enroladinho de salsicha',
        'cuca de uva'
    ]
     
    if (!comLactose) {
        cardapio.push('pastel de queijo');
    }

    /* if (typeof cardapio.concat !== 'function') {
        cardapio = {}, cardapio.concat = () => []
    } */
    /* cardapio = cardapio.concat([
        'pastel de carne',
        'empada de legumes marabijosa'
    ]); */

    //Spread para destruir algo e concatenar com o que você quer.
    cardapio = [...cardapio,'pastel de carne', 'empada de legumes marabijosa']; 

    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        cardapio.splice(cardapio.indexOf( 'enroladinho de salsicha' ), 1)
        cardapio.splice(cardapio.indexOf( 'pastel de carne' ), 1)
    }

    let resultado = cardapio.map(alimento => alimento.toUpperCase());

    /* let resultado = cardapio
                    .filter(alimento => alimento.length > 12)
                    .map(alimento => alimento.toUpperCase()); */

    /* let resultadoFinal = []

    for (let i = 0; i < cardapio.length; i++) {
        resultadoFinal[i] = cardapio[i].toUpperCase()
    } */
    
    return resultado;
}

// console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]