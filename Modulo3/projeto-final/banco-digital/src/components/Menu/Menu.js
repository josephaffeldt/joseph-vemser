import React from 'react';
import { Link } from 'react-router-dom';

export default function Menu() {
    return (
        <React.Fragment>
            <ul className="clearfix">
                <li><Link to="/agencias">Agencias</Link></li>
                <li><Link to="/contas">Contas</Link></li>
                <li><Link to="/clientes">Clientes</Link></li>
                <li><Link to="/contas-dos-clientes">Contas Dos Clientes</Link></li>
            </ul>
        </React.Fragment>
    );
}