import React from 'react';
import './button.css'

function Button(props) {
    return (
        <React.Fragment>
            <button className={props.class} onClick={props.onClick}> {props.texto} </button>
        </React.Fragment>
    )
}

export default Button;
