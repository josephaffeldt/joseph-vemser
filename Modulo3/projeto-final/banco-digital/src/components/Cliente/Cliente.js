
export default class Cliente {
    
    constructor(id, nome, cpf, agencia){
        this.id = id
        this.nome = nome
        this.cpf = cpf
        this.agencia = agencia
    }
}