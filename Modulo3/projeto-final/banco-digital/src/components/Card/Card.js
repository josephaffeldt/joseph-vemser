import React from 'react'
import './card.css'

function Cards(props) {
    return (
        <React.Fragment>
            <div className="col col-12 col-md-6">
                <article className="box">
                    {props.children}
                </article>
            </div>
        </React.Fragment>
    )
}

export default Cards;