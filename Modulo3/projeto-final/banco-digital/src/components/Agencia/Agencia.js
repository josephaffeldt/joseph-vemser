export default class Agencia {

    constructor(id, codigo, nome, endereco){
        this.id = id
        this.codigo = codigo
        this.nome = nome
        this.endereco = endereco
    }
}