import React from 'react';
import './Header.css'
import Menu from '../Menu/Menu'

import { Link } from 'react-router-dom'

function Header() {
    return (
        <React.Fragment>
            <header className="main-header">
                <nav>
                    <div className="logo">
                        <Link className="logo-link" to="/"> Digital Bank </Link>
                    </div>
                    <Menu />
                </nav>
            </header>
        </React.Fragment>
    )
}

export default Header;