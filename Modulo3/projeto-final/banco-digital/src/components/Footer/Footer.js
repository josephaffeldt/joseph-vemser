import React from 'react';
import './Footer.css';
import Menu from '../Menu/Menu'


export default function Footer() {
    return (
        <footer className="main-footer">
            <nav>
                <Menu />
            </nav>
        </footer>
    )
}