import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
// import { isAuthenticated } from "../src/ApiServices/Auth";


import LoginPage from './pages/LoginPage/LoginPage';
import Agencias from './pages/AgenciasPage/AgenciasPage';
import Contas from './pages/ContasPage/ContasPage';
import Clientes from './pages/ClientesPage/ClientesPage';
import ContaDosClientes from './pages/ContasDosClientesPage/ContasDosClientesPage'




export default function App() {

  return (
    <Router>
      <Route path="/" exact component={LoginPage} />
      <Route path="/agencias" component={Agencias} />
      <Route path="/contas" component={Contas}/>
      <Route path="/clientes" component ={Clientes}/>
      <Route path="/contas-dos-clientes" component={ContaDosClientes}/>
    </Router>
  );

  /*
  const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );

   return (
    <Router>
      <Route path="/" exact component={LoginPage} />
      <PrivateRoute path="/agencias" component={Agencias} />
      <PrivateRoute path="/contas" component={Contas}/>
      <PrivateRoute path="/clientes" component ={Clientes}/>
      <PrivateRoute path="/contas-dos-clientes" component={ContaDosClientes}/>
    </Router>
  ); */
}