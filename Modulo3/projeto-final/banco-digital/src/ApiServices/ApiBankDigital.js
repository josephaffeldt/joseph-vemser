import axios from "axios";
import { getToken } from "./Auth";

export default class ApiBankDigital {
    constructor() {
        this.api = axios.create({
            baseURL: "http://localhost:1337"
        });

        this.api.interceptors.request.use(async config => {
            const token = getToken();
            if (token) {
              config.headers.Authorization = `${token}`
            }
            return config;
          })
          
        }
    

    agencias = () => this.api.get('/agencias', { 
      headers: { 
        Authorization: getToken() 
      } 
    })
  
    agenciaPorId = (id) => this.api.get(`/agencia/${id}`, {
      headers: {
        Authorization: getToken()
      }
    })
  
    clientes = () => this.api.get('/clientes', {
      headers: {
        Authorization: getToken()
      }
    })
  
    clientePorID = (id) => this.api.get(`/cliente/${id}`, {
      headers: {
        Authorization: getToken()
      }
    })
  
    contas = () => this.api.get('/tipoContas/', {
      headers: {
        Authorization: getToken()
      }
    })
  
    contaPorId = (id) => this.api.get(`/tiposConta/${id}`, {
      headers: {
        Authorization: getToken()
      }
    })
  
    contaXClientes = () => this.api.get('/conta/clientes/', {
      headers: {
        Authorization: getToken()
      }
    })
  
    contaXClientePorId = (id) => this.api.get(`/conta/cliente/${id}`, {
      headers: {
        Authorization: getToken()
      }
    })
  
    logar = (login, pass) => this.api.post('/login', { 'email': login, 'senha':pass });
  
}


