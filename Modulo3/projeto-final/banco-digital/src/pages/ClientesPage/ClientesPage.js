import React, { Component } from "react";
import axios from 'axios';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Cards from '../../components/Card/Card';
import Cliente from '../../components/Cliente/Cliente';

export default class ClientesPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clientes: []
        }
    }

    componentWillMount() {
        axios.get(
            `http://localhost:1337/clientes/`, {
            headers: {
                Authorization: "banco-vemser-api-fake"
            }
        }).then(res => {
            this.setState({
                clientes: res.data.clientes.map(cl => cl = new Cliente(cl.id, cl.nome, cl.cpf, cl.agencia))
            })
        })
            .catch(err => {
                console.log("Erro")
            })
    }

    render() {
        const { clientes } = this.state
        console.log(clientes)
        return (
            <React.Fragment>
                <Header />
                <div className="App">
                    <header className="App-header">
                        <div className="container">
                            <div className="row">
                                <p> Digite o ID para buscar o Cliente </p>
                                <input placeholder="Filtar por id" type="number" />
                            </div>
                            <div className="row">
                                {clientes.map(cl => cl =
                                    <Cards key={cl.id}>
                                        <h1>ID: {cl.id}</h1>
                                        <p>Nome: {cl.nome}</p>
                                        <p>CPF: {cl.cpf}</p>
                                    </Cards>
                                )
                                }
                            </div>
                        </div>
                    </header>
                </div>
                <Footer />
            </React.Fragment>
        );
    }
}