import React, { Component } from "react";
import axios from 'axios';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Cards from '../../components/Card/Card';
import Conta from '../../components/Conta/Conta';

export default class ContasPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            contas: []
        }
    }

    componentWillMount() {
        axios.get(
            `http://localhost:1337/tipoContas`, {
            headers: {
                Authorization: "banco-vemser-api-fake"
            }
        }).then(res => {
            this.setState({
                contas: res.data.tipos.map(ct => ct = new Conta(ct.id, ct.nome))
            })
        })
            .catch(err => {
                console.log("Erro")
            })
    }

    render() {
        const { contas } = this.state
        console.log(contas)
        return (
            <React.Fragment>
                <Header />
                <div className="App">
                    <header className="App-header">
                        <div className="container">
                            <div className="row">
                                <p> Digite o ID para buscar a Conta </p>
                                <input placeholder="Filtar por id" type="number" />
                            </div>
                            <div className="row">
                                {contas.map(ct => ct =
                                    <Cards key={ct.id}>
                                        <h1>ID: {ct.id}</h1>
                                        <h2>Nome: {ct.nome}</h2>
                                    </Cards>
                                )
                                }
                            </div>
                        </div>
                    </header>
                </div>
                <Footer />
            </React.Fragment>
        );
    }
}