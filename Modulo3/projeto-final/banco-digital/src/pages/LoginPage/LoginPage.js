import React, { Component } from "react";
import { Link } from 'react-router-dom';
import ApiServices from '../../ApiServices/ApiBankDigital'
import './login.css';
import Button from '../../components/Button/Button'

export default class LoginPage extends Component {

    constructor(props) {
        super(props)
        this.apiServices = new ApiServices()
        this.state = {
            login: '',
            pass: '',
            error: ''
        }
    }

    setLogin(evt) {
        const login = evt.target.value;
        this.setState({
            login: login
        })
    }

    setPass(evt) {
        const pass = evt.target.value;
        this.setState({
            pass: pass
        })
    }

   /*  fazerLogin = async (e) => {
        e.preventDefault();
        const { login, pass } = this.state;
        console.log(login, pass)

        if (!login || !pass) {
            this.setState({ error: "Preencha todos os campos para continuar!" });
        } else {
            try {
                const response = await this.apiServices.logar(login, pass);
                login(response.data.token);
                this.props.history.push("/agencias");
            } catch (err) {
                this.setState({
                    error: "Verifique suas credenciais."
                });
            }
        }
    } */

    render() {
        const { login, pass } = this.state
        console.log(login)
        console.log(pass)

        return (
            <div className="App">
                <header className="App-header">
                    <div className="Div-Login">
                        <h1> Login </h1>
                        {this.state.error && <p>{this.state.error}</p>}
                        <div>
                            <input type="text" placeholder="Login" onBlur={this.setLogin.bind(this)} />
                            <input type="password" placeholder="Senha" onBlur={this.setPass.bind(this)} />
                        </div>
                        <div>
                        <Link to="/agencias" className="link" >
                                <Button class="btn big-btn" texto="Login" /* onClick={this.fazerLogin.bind(this)} */ />
                        </Link>

                        </div>
                    </div>
                </header>
            </div>
        )
    };

}