import React, { Component } from "react";
import axios from 'axios';
import ApiService from '../../ApiServices/ApiBankDigital';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Cards from '../../components/Card/Card';
import Agencia from '../../components/Agencia/Agencia';
import Button from '../../components/Button/Button'

export default class AgenciasPage extends Component {

    constructor(props) {
        super(props)
        this.apiService = new ApiService()
        this.state = {
            listaDeAgencias: [],
            listaFiltrada: [],
            is_digital: false
        }
    }

    componentWillMount() {
        return this.apiService.agencias().then(res => {
            this.setState({
                listaDeAgencias: res.data.agencias.map(ag => ag = new Agencia(ag.id, ag.codigo, ag.nome, ag.endereco))
            })
        }).catch(err => {
            console.log("Erro")
        })
    }


/*     componentDidMount() {
        this._asyncRequest = this.requestAgencias();
        this._asyncRequest = null;
    }

    componentWillUnmount() {
        if (this._asyncReques) {
            this._asyncRequest.cancel();
        }
    }

    get requestAgencias() {
        console.log(this.apiService.agencias())
        return this.apiService.agencias()
            .then(res => {
                this.setState({
                    listaDeAgencias: res.data.agencias.map(ag => ag = new Agencia(ag.id, ag.codigo, ag.nome, ag.endereco))
                })
            }).catch(err => {
                console.log("Erro")
            })
    }
*/

    filtarAgencia(evt) {
        let id = evt.target.value

        axios.get(`http://localhost:1337/agencia/:id`, {
            headers: {
                Authorization: `banco-vemser-api-fake`
            }
        })
        .then(res => {
            this.setState({
                listaDeAgencias: res.data.agencias.filter(ag => ag.id === id)
            })
        }).catch(err => {
            console.log("Erro")
        })

    }

    render() {
        const { listaDeAgencias } = this.state
        return (
            <React.Fragment>
                <Header />
                <div className="App">
                    <header className="App-header">
                        <div className="container">
                            <div className="row Div-Busca">
                                <p> Digite o ID para buscar a Agência </p>
                                <input placeholder="Filtar por id" type="number" onBlur={this.filtarAgencia.bind(this)} />
                            </div>
                            <div className="row">
                                {listaDeAgencias.map(ag => ag =
                                    <Cards key={ag.id}>
                                        <h1>ID: {ag.id} </h1>
                                        <p>Codigo: {ag.codigo} </p>
                                        <p>Nome: {ag.nome} </p>
                                        <p></p>
                                        <p>ENDEREÇO:</p>
                                        <p>{ag.endereco.logradouro}</p>
                                        <p>Nº: {ag.endereco.numero}</p>
                                        <p>Bairro: {ag.endereco.bairro} </p>
                                        <p>Cidade: {ag.endereco.cidade}</p>
                                        <p>UF: {ag.endereco.uf}</p>

                                        <Button class="btn big-btn" texto="É Digital?" />
                                    </Cards>
                                )}
                            </div>

                        </div>
                    </header>
                </div>
                <Footer />
            </React.Fragment>
        );
    }
}