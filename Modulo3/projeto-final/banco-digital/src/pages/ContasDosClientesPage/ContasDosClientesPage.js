import React, { Component } from "react";
import axios from 'axios';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Cards from '../../components/Card/Card';
import ContaDoCliente from '../../components/ContaDoCliente/ContaDoCliente'

export default class ContasDosClientesPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contasClientes: []
        }
    }

    componentWillMount() {
        axios.get(
            `http://localhost:1337/conta/clientes`, {
            headers: {
                Authorization: "banco-vemser-api-fake"
            }
        }).then(res => {
            this.setState({
                contasClientes: res.data.cliente_x_conta.map(ct => ct = new ContaDoCliente(ct.id, ct.codigo, ct.tipo, ct.cliente))
            })
        })
            .catch(err => {
                console.log("Erro")
            })
    }

    render() {
        const { contasClientes } = this.state
        console.log(contasClientes)
        return (
            <React.Fragment>
                <Header />
                <div className="App">
                    <header className="App-header">
                        <div className="container">
                            <div className="row">
                                <p> Digite o ID para buscar a Conta </p>
                                <input placeholder="Filtar por id" type="number" />
                            </div>
                            <div className="row">
                                {contasClientes.map(ct => ct =
                                    <Cards key={ct.id}>
                                        <h1>ID: {ct.id}</h1>
                                        <p>Codigo: {ct.codigo}</p>
                                        <p>cliente: {ct.cliente.nome}</p>
                                    </Cards>
                                )
                                }
                            </div>
                        </div>
                    </header>
                </div>
                <Footer />
            </React.Fragment>
        );
    }
}