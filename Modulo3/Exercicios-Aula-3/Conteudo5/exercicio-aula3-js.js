// padrões de eventos (Window or Document)
let nome = document.getElementById('primeiroNome');
let email = document.getElementById('email');
let sobreNome = document.getElementById('sobreNome');
let tel = document.getElementById('tel');
let cpf = document.getElementById('cpf');
let dn = document.getElementById('dn');
let areaTexto = document.getElementById('areaTexto');
let bt = document.getElementById('button');

nome.addEventListener('blur', () => {
    if (nome.value.length < 10) {
        alert("O nome deve possuir 10 caracteres.");
    }
})

email.addEventListener('blur', () => {
    if (!email.value.includes('@')) {
        alert("E-mail Inválido");
    }
});

bt.addEventListener('click', () => {
    if (nome.value.length == 0 ||
        email.value.length == 0 ||
        sobreNome.value.length == 0 ||
        tel.value.length == 0 ||
        cpf.value.length == 0 ||
        dn.value.length == 0 ||
        areaTexto.value.length == 0
    ) {
        alert("Todos os campos devem ser preenchidos.");
        event.preventDefault();
    }
})