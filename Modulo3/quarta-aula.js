// Classes em JavaScript

class Jedi {
    constructor(nome) {
        this.nome = nome;
    }

    atacarComSabre() {
        setTimeout(() => {
            console.log(`${this.nome} atacou com sabre!`);
        }, 1000);
    }

    //para trabalhar com o this no escopo local do documento.
    atacarComSabreSelf() {
        let self = this;
        setTimeout(function () {
            console.log(`${self.nome} atacou com sabre 2!`);
        }, 1000);
    }
}
let luke = new Jedi("Luke");

/* console.log(luke);
luke.atacarComSabre();
luke.atacarComSabreSelf() */

// Promessas usado para chamadas de api

/* let defer = new Promise((resolve, reject) => {
    setTimeout(() => {
        if (true) {
            resolve('Foi Resolvido');
        }
        else {
            reject('Erro');
        }
    }, 2000)

});

defer
    .then((data)=> {
        console.log(data)
        return "Novo Resultado"
    })
    .then((data) => console.log(data))
    .catch((erro) => console.log(erro)); */

// Fat para chamada de api externa

let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`);
pokemon
    .then( data => data.json())
    .then( data => console.log(data.results)); 
