import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css'

import Mirror from './Mirror';
import JsFlix from './JsFlix';
import Home from './Home';
import ListaDeAvaliacoes from './components/ListaDeAvaliacoes'
import TelaDetalheEpisodio from './components/TelaDetalheEpisodio'

export default class App extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/mirror" component={Mirror} />
        <Route path="/jsflix" component={JsFlix} />
        <Route path="/avaliacoes" component={ListaDeAvaliacoes} />
        <Route path="/episodio/:id" component={TelaDetalheEpisodio}/>
      </Router>
    );
  }
}
