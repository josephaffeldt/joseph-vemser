import React, { Component } from 'react';
import './App.css';
import ListaSeries from './models/ListaSeries';
import SeriesUI from './components/SeriesUI';

export default class JsFlix extends Component {
  constructor(props) {
    super(props);
    this.listaSeries = new ListaSeries()
    this.state = {
      listaSeries: this.listaSeries,
      deveExibir: false
    }
  }

  gera(evt) {
    const ano = evt.target.value
    const { listaSeries } = this.state
    listaSeries.filtrarPorAno(ano)

    this.setState({
      listaSeries: listaSeries.filtrarPorAno(ano),
      deveExibir: true
    })
  }

  render() {
    const { listaSeries, deveExibir } = this.state

    return (
      <div className="App">
        <header className='App-header'>
          <input placeholder="Digite uma serie valida" onBlur={this.gera.bind(this)} />
          <SeriesUI listaSeries={listaSeries} deveExibir={deveExibir} />
        </header>
      </div>
    );
  }
}