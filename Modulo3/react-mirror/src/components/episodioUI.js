import React, { Component } from 'react';

export default class EpisodioUI extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        const { episodio } = this.props;
        return (
            <React.Fragment>
                <h2>{episodio.nome}</h2>
                <img src={episodio.url} alt={episodio.nome}></img>
                <span>Assistido? {episodio.assistido ? 'Sim' : 'Não'}, {episodio.qtdVezesAssistido} vezes</span>
                <span>{episodio.duracaoEmMin}</span>
                <span>{episodio.temporadaEpisodio}</span>
                <span>Nota: {episodio.nota}</span>
            </React.Fragment>
        );
    }
}