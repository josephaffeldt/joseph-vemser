import React, { Component } from 'react';

export default class SeriesUI extends Component {
    render() {
        const { listaSeries, deveExibir } = this.props;
        return deveExibir ? (
            <React.Fragment>
                <h2>Titulo: {listaSeries.map(serie => serie.titulo).join(' - ')}</h2>
                <span> Elenco: {listaSeries.map(serie => serie.elenco).join(' - ')}</span>
                <span> Diretor: {listaSeries.map(serie => serie.diretor).join(' - ')}</span>
            </React.Fragment>
        ) : null;
    }
}