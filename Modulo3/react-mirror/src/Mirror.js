import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUI from './components/episodioUI';
import MensagemFlash from './components/MensagemFlash';
import { Link } from 'react-router-dom';
import MeuInputNumero from './components/MeuInputNumero';


export default class Mirror extends Component {
  constructor(props) {
    super(props);
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  assistido() {
    const { episodio } = this.state;
    episodio.marcarParaAssistido();
    this.setState({
      episodio
    });
  }

  //Correção Marcos
  darNota({ nota, erro }) {
    this.setState({
      deveExibirErro: erro
    })

    if (erro) {
      return;
    }

    let cor, mensagem;
    const { episodio } = this.state
    if (episodio.validarNota(nota)) {
      episodio.avaliar(nota)
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    } else {
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'
    }

    this.exibirMensagem({ cor, mensagem })
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  /* 
    notaInvalida() {
      const { deveExibirMensagem } = this.state
      return (
        <div>
          {
            (this.state.episodio.nota > 5 || this.state.episodio.nota > 5) && (
              <div className="msg">
                <MensagemFlash atualizarMensagem={this.atualizarMensagem}
                  deveExibirMensagem={deveExibirMensagem}
                  mensagem="Informar uma nota válida (entre 1 e 5)" cor="vermelho" />
              </div>
            )
          }
        </div>
      )    
    } */


  /* geraCampoDeNota() {
    // https://reactjs.org/docs/conditional-rendering.html
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <input type="number" placeholder="Digite uma nota de 1 a 5  " onBlur={this.darNota.bind(this)} ></input>
            </div>
          )
        }
      </div>
    )
  } */

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state
    const { listaEpisodios } = this
    return (
      <div className="App">
        <MensagemFlash atualizarMensagem={this.atualizarMensagem}
          deveExibirMensagem={exibirMensagem}
          mensagem={mensagem}
          cor={cor}
          segundos={5} />
        <header className='App-header'>
          <EpisodioUI episodio={episodio}></EpisodioUI>
          {/* {this.geraCampoDeNota()} */}
          <MeuInputNumero placeholder="1 a 5"
            msgCampo="Qual sua nota para esse episódio?"
            visivel={episodio.assistido || false}
            obrigatorio={true}
            atualizarValor={this.darNota.bind(this)}
            deveExibirErro={deveExibirErro}
          />
          <div className='botao'>
            <button className='btn verde' onClick={this.sortear.bind(this)}>Próximo</button>
            <button className='btn azul' onClick={this.assistido.bind(this)}>Já Assisti</button>
            <Link to='./jsflix' className="link">
              <button className='btn verde'> JsFlix </button>
            </Link>
            <Link to={{ pathname: './avaliacoes', state: { listaEpisodios } }} className="link">
              <button className='btn vermelho'>Avaliações</button>
            </Link>
          </div>
        </header>
      </div>
    );
  }
}