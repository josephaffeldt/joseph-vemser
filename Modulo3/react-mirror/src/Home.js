import React, { Component } from 'react';
import { Link } from 'react-router-dom';  

export default class Home extends Component {
  render() {
    return (
        <div className="Home-style">
            <Link to="/mirror" className="link"> <button className='btn verde'>  React Mirror </button></Link>
            <Link to="/jsflix" className="link"> <button className='btn azul'>  Info Jsflix </button></Link>
        </div>
    );
  }
}