/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import '../css/box.css';
import Texto from './texto-section';
import Button from './button';
// import img1 from '../img/Smartsourcing.png';

function Cards(props) {
    return (
        <React.Fragment>
            <div className={props.col}>
                <article className={props.box}>
                    <div>
                        <img src={props.imagem}/>
                    </div>
                    <Texto titulo={props.titulo} paragrafo={props.paragrafo}/>
                    <Button class={props.class} texto={props.texto}/>
                </article>
            </div>
        </React.Fragment>
    )
}

export default Cards;