import React from 'react';
import '../css/reset.css';
import { Link } from 'react-router-dom';

export default function Menu() {
    return (
        <React.Fragment>
            <ul className="clearfix">
                <li><Link to="/">Home</Link></li>
                <li><Link to="/sobrenos">Sobre nós</Link></li>
                <li><Link to="/servicos">Serviços</Link></li>
                <li><Link to="/contato">Contato</Link></li>
            </ul>
        </React.Fragment>
    );
}