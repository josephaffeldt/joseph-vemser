/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import Texto from './texto-section';
import '../css/grid.css';
import '../css/sobrenos.css';


export default function CardSobreLeft(props) {
    return (
        <React.Fragment>
            <div className="row bitbox">
                <div className={props.divText}>
                    <Texto titulo={props.titulo} paragrafo={props.p1} />
                    <p>{props.p2}</p>
                </div>
                <div className={props.divImg}>
                    <img src={props.imagem} />
                </div>
            </div>
        </React.Fragment >
    )
}