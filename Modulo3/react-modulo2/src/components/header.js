import React from 'react';
import '../css/header.css';
import '../css/grid.css';
import logoDbc from '../img/logo-dbc-topo.png';
import Menu from './menu'

import { Link } from 'react-router-dom'

function Header () {
    return (
        <React.Fragment>
            <header className="main-header">
            <nav className="container clearfix">
                <Link className="logo" to="/">
                    <img src={logoDbc} alt="DBC Company"></img>
                </Link>    
                
                <label className="mobile-menu" for="mobile-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>

                <input id="mobile-menu" type="checkbox"></input>

                <Menu/>
            </nav> 
        </header>
        </React.Fragment>
    )
}

export default Header;