import React from 'react';

export default function Texto(props) {
    return (
        <React.Fragment>
                <h2> {props.titulo} </h2>
                <p> {props.paragrafo}</p>
        </React.Fragment>
    )
}