/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import Texto from './texto-section';

export default function CardSobreLeft(props) {
    return (
        <React.Fragment>
            <div className="row bitbox">
                <div className={props.divImg}>
                    <img src={props.imagem} />
                </div>
                <div className={props.divText}>
                    <Texto titulo={props.titulo} paragrafo={props.p1} />
                    <p>{props.p2}</p>
                </div>
            </div>
        </React.Fragment >
    )
}