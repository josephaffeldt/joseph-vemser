/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import '../css/box.css';
import '../css/grid.css';

export default function CardServico(props) {
    return (
        <React.Fragment>
            <div className={props.col}>
                <article className={props.box}>
                    <div>
                        <img src={props.imagem}/>
                    </div>
                    <p>{props.p}</p>
                </article>
            </div>
        </React.Fragment>
    )
}
