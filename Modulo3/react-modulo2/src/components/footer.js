import React from 'react';
import '../css/footer.css';
import Menu from './menu'


export default function Footer(){
    return (
        <footer class="main-footer">
            <div class="container">
                <nav>
                    <Menu/>
                </nav>
                <p>
                    &copy; Copyright DBC Company - 2019
                </p>
            </div>            
        </footer>
    )
}


