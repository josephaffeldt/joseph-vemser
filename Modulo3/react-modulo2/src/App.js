import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './Home';
import SobreNos from './SobreNos';
import Servicos from './Servicos';
import Contato from './Contato';

class App extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/sobrenos" component={SobreNos}/>
        <Route path="/servicos" component={Servicos}/>
        <Route path="/contato" component={Contato}/>
      </Router>
    );
  }

}

export default App;
