import React, { Component } from 'react';
import './css/reset.css';
import './css/grid.css';
import './css/banner.css'
import Header from './components/header';
import Footer from './components/footer';
import Button from './components/button';
import Texto from './components/texto-section';
import Card from './components/card';
import img1 from './img/Smartsourcing.png';
import img2 from './img/Software-Builder.png';
import img3 from './img/Sustain.png';
import img4 from './img/Agil.png';

const parag = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Neque alias excepturi molestiae distinctio ipsum odit dolorum, at minima quia vel in quidem, velit sit tenetur voluptas nemo doloribus? Ab, architecto?";

export default class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />

        <section className="main-banner space   ">
          <article>
            <h1> Vem Ser DBC </h1>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam repudiandae labore modi ratione. Tempore, impedit nulla. Deleniti sint, recusandae culpa minus eligendi, aspernatur iure, eum ea dicta dolore sunt obcaecati.
            </p>
            <Button class="button button-big button-outline" texto="Saiba mais" />
          </article>
        </section>

        <section className="container">
          <div className="row">
            <div className="col col-12 col-md-7 col-lg-7">
              <Texto titulo="Titulo UM" paragrafo={`${parag}
              ${parag}`} />
            </div>
            <div className="col col-12 col-md-5 col-lg-5">
              <Texto class="col col-12 col-md-5 col-lg-5" titulo="Titulo DOIS" paragrafo={parag} />
            </div>
          </div>

          <div className="row">
            <Card col="col col-12 col-md-3 col-lg-3" box="box" imagem={img1} titulo="Titulo UM" paragrafo={parag} class="button button-green" texto="Saiba Mais" />
            <Card col="col col-12 col-md-3 col-lg-3" box="box" imagem={img2} titulo="Titulo DOIS" paragrafo={parag} class="button button-blue" texto="Saiba Mais" />
            <Card col="col col-12 col-md-3 col-lg-3" box="box" imagem={img3} titulo="Titulo TRES" paragrafo={parag} class="button button-green" texto="Saiba Mais" />
            <Card col="col col-12 col-md-3 col-lg-3" box="box" imagem={img4} titulo="Titulo QUATRO" paragrafo={parag} class="button button-blue" texto="Saiba Mais" />
          </div>
        </section>

        <Footer />
      </React.Fragment>
    );
  }

}