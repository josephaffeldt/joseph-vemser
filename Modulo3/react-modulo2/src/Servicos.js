import React, { Component } from 'react';
import Header from './components/header';
import Footer from './components/footer';
import imgFem from './img/perfilfem.png';
import imgMasc from './img/perfilmasc.png'

import CardServ from './components/cardServicos'
import Button from './components/button'

const parag = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Neque alias excepturi molestiae distinctio ipsum odit dolorum, at minima quia vel in quidem, velit sit tenetur voluptas nemo doloribus? Ab, architecto?";


export default class Servicos extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <section className="container space">
                    <div className="row">
                        <CardServ col="col col-12 col-md-4 col-lg-4" box="box3" imagem={imgFem} p={parag} />
                        <CardServ col="col col-12 col-md-4 col-lg-4" box="box3" imagem={imgMasc} p={parag} />
                        <CardServ col="col col-12 col-md-4 col-lg-4" box="box3" imagem={imgFem} p={parag} />
                    </div>
                    <div className="row">
                        <CardServ col="col col-12 col-md-4 col-lg-4" box="box3" imagem={imgMasc} p={parag} />
                        <CardServ col="col col-12 col-md-4 col-lg-4" box="box3" imagem={imgFem} p={parag} />
                        <CardServ col="col col-12 col-md-4 col-lg-4" box="box3" imagem={imgMasc} p={parag} />
                    </div>

                    <div class="row center">
                        <article class="col col-12">
                            <h1> TITULO</h1>
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ex quasi laudantium saepe aliquid. Hic, vitae, ab laudantium eius minima maiores quisquam, incidunt voluptatum ipsa delectus illo facere sed corrupti error.
                            </p>
                            <p>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quae delectus praesentium similique animi voluptatem, saepe accusamus repellat eaque natus quibusdam consequuntur aliquid voluptates exercitationem maxime ducimus quia modi deserunt officiis?
                            </p>
                            <Button class="button button-blue" texto="Button"/>
                        </article>
                    </div>
                </section>
                <Footer />
            </React.Fragment>
        );
    }
}