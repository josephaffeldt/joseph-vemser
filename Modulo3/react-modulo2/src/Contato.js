import React, { Component } from 'react';
import Header from './components/header';
import Footer from './components/footer';

export default class Contato extends Component {
    render(){
        return(
            <React.Fragment>
                <Header />
                    
                <Footer />
            </React.Fragment>
        );
    }
}