import React, { Component } from 'react';
import Header from './components/header';
import Footer from './components/footer';
import CardL from './components/cardSobreLeft';
import CardR from './components/cardSobreRight';
import imgMasc from './img/perfilmasc.png'
import imgFem from './img/perfilfem.png'

const parag = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae aliquam magni, asperiores doloribus odio earum! Totam, aperiam, error dolores architecto labore similique esse nemo illo commodi veniam pariatur nobis minus!";

export default class SobreNos extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <section class="container main-sobrenos ">
                    <CardL divImg="col col-12 col-md-2 col-lg-2" imagem={imgFem} divText="sobre-nos-div col col-12 col-md-10 col-lg-10" titulo="Titulo UM" p1={parag} p2={parag}/>
                    <CardR divImg="col col-12 col-md-2 col-lg-2" imagem={imgMasc} divText="col col-12 col-md-10 col-lg-10" titulo="Titulo DOIS" p1={parag} p2={parag}/>
                    <CardL divImg="col col-12 col-md-2 col-lg-2" imagem={imgFem} divText="sobre-nos-div col col-12 col-md-10 col-lg-10" titulo="Titulo TRES" p1={parag} p2={parag}/>
                    <CardR divImg="col col-12 col-md-2 col-lg-2" imagem={imgMasc} divText="col col-12 col-md-10 col-lg-10" titulo="Titulo QUATRO" p1={parag} p2={parag}/>
                </section>
                <Footer />
            </React.Fragment>
        );
    }
}