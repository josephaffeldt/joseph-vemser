
//  Função proposta pelo Marcos
/* function adicionar (op1){
    return function(op2){
        return op1 + op2;
    }
} */

//  Função proposta pelo Marcos
//let adicionar = op1 => op2 => op1 + op2;

//  Função para verificar se um número é divisivel
/* const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

//  Com o Curry
/* const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12)); */

/* console.log("Exercicio 4:");
console.log(adicionar(8)(2.5));
console.log(adicionar(5642)(8749));
console.log("\n"); */