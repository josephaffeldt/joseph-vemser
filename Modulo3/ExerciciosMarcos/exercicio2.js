
//  Função proposta pelo Marcos
function anoBissexto(ano) {
    return (ano % 400 === 0) || (ano % 400 === 0 && ano % 100 !== 0) ? true : false;
}
//  Função 2 proposta pelo Marcos
/* const teste = {
        diaAula:    "Segunda",
        local:      "DBC",
        bissexto(ano){
            return (ano % 400 === 0) || (ano %400 === 0 && ano %100 !== 0) ? true : false;
        }
    }
    console.log(teste.bissexto(2015)); */


//  Função 3 proposta pelo Marcos (Arrow Function)
/*  let bissexto = ano => (ano % 400 === 0) || (ano %400 === 0 && ano %100 !== 0) ? true : false;
    console.log(bissexto(2016)) */


/* console.log("Exercicio 2:");
console.log(anoBissexto(1900));
console.log(anoBissexto(2018));
console.log(anoBissexto(2000));
console.log(anoBissexto(2020));
console.log("\n"); */