function criarSanduiche(pao, recheio, queijo, salada) {
    console.log(`
        Seu Sanduiche tem o pão ${pao} 
        com delicioso recheio de ${recheio} 
        e quejo ${queijo} 
        com salada de ${salada}`);
}

const ingredientes = ['3 quejos', 'Frango', 'Cheddar', 'Tomate e Alface'];

// Chamado em 40 lugares
// criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

// receberValoresIndefinidos("um", "dois","tres")

// console.log([..."Marcos"]);


// *** EVENTOS ***
// padrões de eventos (Window or Document)
let inputTest = document.getElementById('campoTest');
inputTest.addEventListener('blur', () => {
    alert("Obrigado");
})