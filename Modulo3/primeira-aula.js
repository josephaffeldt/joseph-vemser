//console.log("Cheguei", "Aqui");

var nomeDaVariavel = "valor"; //Valor para global sem var
let nomeDaLet = "ValorLet"; //ES6 - para parametros locais;
const nomeDaConst = "ValorDaConst"; //ES6 valores constantes não mudam

var nomeDaVariavel = "Valor3";
nomeDaLet = "ValorLet2";

const nomeDaConst2 = { //É um objeto, para criar objetos basta colocar {} (Colchetes).
    nome: "Joseph",
    idade: 25
};

Object.freeze(nomeDaConst2);//Freeza qualquer tipo, ou seja, torna ela totalmente imutavel

nomeDaConst2.nome = "Joseph Weber"; //Forma de mudar a constante, acessar o parametro do objeto constante por ponto.

//console.log(nomeDaConst2);

function nomeDeFuncao() {
    //nomeDaVariavel = "valor";
    //let nomeDaLet = "ValorLet2";
}

//console.log(nomeDaLet);

//function somar(){
//}

function somar(valor1, valor2 = 1) {
    console.log(valor1 + valor2);
}

//somar(3); //assume que o valor2 passado é o defaul colocado na assinatura da function
//somar(3,2); //passa os dois valores da assinatura.

// console.log(1+1);
// console.log(1+"1"); // concatena, pois não há somas (exceto de bits) entre strings

function ondeMoro(cidade) {
    // console.log("Eu moro em " + cidade + ". E sou feliz");
    console.log(`Eu moro em ${cidade}. E sou feliz ${20 + 5} anos.`); //Crase mais ${  } faz a concatenação das strings diretamente sem precisar do +;
}

//ondeMoro("Porto Alegre");

function fruteira() {
    let texto =
        "Banana"
        + "\n"
        + "Ameixa"
        + "\n"
        + "Tangerina"
        + "\n"
        + "Pêssego"
        + "\n";

    let newTexto = `
        Banana
            Ameixa
                Tangerina
                    Pêssego
    `;

    console.log(texto);
    console.log(newTexto);
}

//fruteira();

let eu = {
    nome: "Joseph",
    idade: 25,
    altura: 1.73
};

function quemSou(pessoa) {
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura} de altura`);
}

// quemSou(eu);

let funcaoSomarValores = function (a, b) {
    return a + b;
};

let add = funcaoSomarValores;
let resultado = add(3, 5); //chama a função add que foi nomeada para adicionar, é usado quanto há funções genéricas.
// console.log(resultado);

const { nome: n, idade: i } = eu; //pega o nome e a idade do objeto chamado eu (deve passar o parametro do objeto que você quer entre colchetes)
// console.log(nome, idade);
// console.log(n, i);
// console.log(eu);

const array = [1, 2, 3, 4, 8];
const [n1, , n2, n3, n4, n5 = 18] = array;
// console.log(n1, n2, n3, n5);

function testarPessoa({ nome, idade, altura }) {
    console.log(nome, idade, altura);
}

// testarPessoa(eu);

let a1 = 42;
let b1 = 15;
let c1 = 99;
let d1 = 109;

// console.log(a1, b1, c1, d1);
[a1, b1, c1, d1] = [b1, d1, a1, c1]; // quebra e realiza a troca.
// console.log(a1, b1, c1, d1);