class PokeApi { // eslint-disable-line no-unused-vars
  async buscarTodos() {
    const fazRequisicao = fetch( 'https://pokeapi.co/api/v2/pokemon' );
    const resultadoEmString = await fazRequisicao;
    return resultadoEmString.json();
  }

  async buscar( id ) {
    const fazRequisicao = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` );
    const resultadoEmString = await fazRequisicao;
    return resultadoEmString.json();
  }
}
