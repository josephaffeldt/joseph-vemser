const pokeApi = new PokeApi();

function renderizacaoPokemon( pokemon ) {
  const texto = document.getElementById( 'areaTexto' )
  const imagem = document.getElementById( 'imagem-pokemon' )
  texto.innerHTML = pokemon.formataPokemon();
  imagem.src = pokemon.imagem;
}

async function buscar( id ) {
  const pokemonEspecifico = await pokeApi.buscar( id );
  const poke = new Pokemon( pokemonEspecifico );
  renderizacaoPokemon( poke );
}

const erro = document.getElementById( 'erro' );
const pesquisa = document.getElementById( 'pesquisa-pokemon' )
const botao = document.getElementById( 'botao' )
let valorDigitado = 0;

botao.addEventListener( 'click', () => {
  erro.innerText = '';
  let sorteado = Math.round( ( Math.random() * 801 ) ) + 1;
  while ( localStorage.getItem( sorteado ) ) {
    sorteado = Math.round( ( Math.random() * 801 ) ) + 1;
  }
  buscar( sorteado )
  localStorage.setItem( sorteado, sorteado )
} )

pesquisa.addEventListener( 'blur', () => {
  if ( pesquisa.value !== valorDigitado ) {
    if ( pesquisa.value >= 1 && pesquisa.value <= 802 ) {
      valorDigitado = pesquisa.value;
      erro.innerText = '';
      buscar( pesquisa.value )
    } else {
      erro.innerText = 'Digite um pokemon válido!';
    }
  }
} )
