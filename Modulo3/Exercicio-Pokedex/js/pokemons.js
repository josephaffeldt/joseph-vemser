class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name.toUpperCase();
    this.id = obj.id;
    this.altura = obj.height;
    this.peso = obj.weight;
    this.imagem = obj.sprites.front_default;
    this.tipos = obj.types.map( data => data.type.name )
    this.stats = obj.stats.map( data => `${ data.stat.name } : ${ data.base_stat } \n` );
  }

  formataPokemon() {
    return `***   INFO   ***
NOME :   ${ this.nome }
ID :   ${ this.id }
ALTURA :   ${ this.altura * 10 } cm
PESO :   ${ this.peso / 10 } Kg
TIPO : ${ this.tipos.map( data => ` ${ data }` ) }

***   STATS   *** 
${ this.stats }
`;
  }
}
