/* eslint-disable no-useless-constructor */
import React, { Component } from 'react';
import './App.css';

// import CompA, { CompB } from './components/ExemploComponenteBasico'
import Membros from './components/Membros';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <Membros nome="Joseph" sobrenome="Weber"/>
        <Membros nome="Jasi" sobrenome="Cardoso"/>
        <Membros nome="Alice" sobrenome="Affeldt"/>
        {/* <CompA />
        <CompA />
        <CompA />
        <CompA />
        <CompB />
        <CompB />
        <CompB /> */}
      </div>
    );
  }

}

export default App;
