import React from 'react';

export default (props) =>
    <React.Fragment>
        {props.nome} {props.sobrenome}
    </React.Fragment>

/* [
    props.nome,
    props.sobrenome
]
   // eslint-disable-next-line no-unused-expressions

   <div>
    {props.nome} {props.sobrenome}
</div>
*/