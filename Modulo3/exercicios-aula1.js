//Exercicio 1
let circulo1 = {
    raio: 3,
    tipoCalculo: 'A',
};
let circulo2 = {
    raio: 2,
    tipoCalculo: 'C',
}
function calculaCirculo({ raio, tipoCalculo }) {
    if (tipoCalculo == 'A') {
        return Math.PI * Math.pow(raio, 2);
    }
    else {
        return 2 * (Math.PI * raio);
    }
    return null;
}
/* console.log("Exercicio 1:");
console.log(calculaCirculo(circulo1));
console.log(calculaCirculo(circulo2));
console.log("\n"); */



// Exercicio 2
function anoBissexto(ano) {
    if ((((ano % 4) == 0) && ((ano % 100) != 0)) || (((ano % 4) == 0) && ((ano % 400) == 0))) {
        return true;
    }
    return false;
}
/* console.log("Exercicio 2:");
console.log(anoBissexto(1900));
console.log(anoBissexto(2018));
console.log(anoBissexto(2000));
console.log(anoBissexto(2020));
console.log("\n"); */



// Exercicio 3
function somaPares(array) {
    let soma = 0;
    for (let i = 0; i < array.length; i++) {
        if (i % 2 == 0) {
            soma = array[i] + soma;
        }
    }
    return soma;
}
/* console.log("Exercicio 3:");
console.log(somaPares([1, 56, 4.34, 6, -2]))
console.log("\n"); */



// Exercicio 4
function adicionar(valor1) {
    let segundoNum = function segundoValor(valor2) {
        return valor2 + valor1;
    };
    return segundoNum;
};
/* console.log("Exercicio 4:");
console.log(adicionar(8)(2.5));
console.log(adicionar(5642)(8749));
console.log("\n"); */


//Exercicio 5
function imprimirBRL(valor, precisao = 2) {
    const fator = Math.pow(10, precisao);
    let val = Math.ceil(valor * fator) / fator;

    let num = val.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
    return num;
}
/* console.log("Exercicio 5:");
console.log(imprimirBRL(4.651)); // "RS 4,66"
console.log(imprimirBRL(0)) // “R$ 0,00”
console.log(imprimirBRL(3498.99)) // “R$ 3.498,99”
console.log(imprimirBRL(-3498.99)) // “-R$ 3.498,99”
console.log(imprimirBRL(2313477.0135)) // “R$ 2.313.477,02”
console.log("\n"); */
