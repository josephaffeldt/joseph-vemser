package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService{

    @Autowired
    private BancoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Banco salvar( Banco banco ){
        return repository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public Banco editar( Banco banco, Integer id ){
        banco.setCodigoBanco(id);
        return repository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById( id );
    }

    public List<Banco> todosBancos(){
        return repository.findAll();
    }

    public Banco bancoEspecifico( Integer id ){
        Optional<Banco> banco = repository.findById(id);
        return banco.get();
    }

    public List<Banco> todosPorNome( String nome ) {
        return repository.findAllByNome(nome);
    }
}
