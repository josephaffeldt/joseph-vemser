package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "AGENCIAS" )
public class Agencias {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_AGENCIAS", sequenceName = "SEQ_AGENCIAS" )
    @GeneratedValue( generator = "SEQ_AGENCIAS", strategy = GenerationType.SEQUENCE )

    @Column( name = "CODIGO_AGENCIA", nullable = false  )
    private Integer codigoAgencia;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_CODIGO_BANCO", nullable = false )
    private Banco codigoBanco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_CIDADE", nullable = false )
    private Cidades idCidade;

    public Integer getCodigoAgencia() {
        return codigoAgencia;
    }
    public void setCodigoAgencia(Integer codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public Banco getCodigoBanco() {
        return codigoBanco;
    }
    public void setCodigoBanco(Banco codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public Cidades getIdCidade() {
        return idCidade;
    }
    public void setIdCidade(Cidades idCidade) {
        this.idCidade = idCidade;
    }
}
