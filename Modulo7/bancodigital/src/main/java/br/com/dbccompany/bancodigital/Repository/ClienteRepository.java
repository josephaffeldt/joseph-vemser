package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<Clientes, Integer> {

    List<Clientes> findAllByNome(String nome);
    Clientes findByCpf( Integer cpf );
    List<Clientes> findAll();


}
