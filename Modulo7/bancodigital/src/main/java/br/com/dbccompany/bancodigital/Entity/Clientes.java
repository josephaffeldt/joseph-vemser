package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table( name = "CLIENTES" )
public class Clientes {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "SEQ_CLIENTES", sequenceName = "SEQ_CLIENTES" )
    @GeneratedValue( generator = "SEQ_CLIENTES", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID_CLIENTE", nullable = false )
    private Integer idCliente;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "CPF", nullable = false )
    private Integer cpf;

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable( name = "CLIENTES_X_CONTAS",
            joinColumns = { @JoinColumn ( name = "ID_CLIENTE" ) },
            inverseJoinColumns = { @JoinColumn ( name = "FK_ID_CONTA" ) })
    private List<Contas> contas = new ArrayList<>();

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable( name = "CLIENTES_X_CIDADES",
            joinColumns = { @JoinColumn ( name = "ID_CLIENTE" ) },
            inverseJoinColumns = { @JoinColumn ( name = "FK_ID_CIDADE" ) })
    private List<Cidades> cidades = new ArrayList<>();

    public Integer getIdCliente() {
        return idCliente;
    }
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCpf() {
        return cpf;
    }
    public void setCpf(Integer cpf) {
        this.cpf = cpf;
    }

    public List<Contas> getContas() {
        return contas;
    }
    public void setContas(List<Contas> contas) {
        this.contas = contas;
    }

    public List<Cidades> getCidades() {
        return cidades;
    }
    public void setCidades(List<Cidades> cidades) {
        this.cidades = cidades;
    }

}
