package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "BANCO" )
public class Banco {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_BANCO", sequenceName = "SEQ_BANCO" )
    @GeneratedValue( generator = "SEQ_BANCO", strategy = GenerationType.SEQUENCE )

    @Column( name = "CODIGO_BANCO", nullable = false )
    private Integer codigoBanco;

    @Column( name = "NOME", nullable = false )
    private String nome;

    public Integer getCodigoBanco() {
        return codigoBanco;
    }
    public void setCodigoBanco(Integer codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getNome() {
        return nome;
    }
}
