package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/banco" )
public class BancoController {

    @Autowired
    BancoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Banco> todosBancos(){
        return service.todosBancos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Banco novoBanco( @RequestBody Banco banco ){
        return service.salvar(banco);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Banco editarBanco( @RequestBody Banco banco, @PathVariable Integer id ){
        return service.editar(banco, id);
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Banco buscarPorId( @PathVariable Integer id ) {
        return service.bancoEspecifico(id);
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public List<Banco> buscarPorNome( @PathVariable String nome) {
        return service.todosPorNome(nome);
    }

    @DeleteMapping( value = "/delete/{id}" )
    @ResponseBody
    public void removerPorId( @PathVariable Integer id ) {
        service.deletar( id );
    }
}
