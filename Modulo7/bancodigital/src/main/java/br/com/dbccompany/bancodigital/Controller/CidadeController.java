package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidades" )
public class CidadeController {

    @Autowired
    CidadeService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Cidades> todosBancos(){
        return service.todasCidades();
    }

    @GetMapping( value = "/cidade/{nome}" )
    @ResponseBody
    public List<Cidades> buscarPorNome( @PathVariable String nome ){
        return service.buscaPorNome(nome);
    }

    @GetMapping( value = "/cidade/{id}" )
    @ResponseBody
    public Cidades buscarPorId( @PathVariable Integer id ){
        return  service.cidadeEspecifica(id);
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Cidades novaCidade( @RequestBody Cidades cidades ){
        return service.salvar(cidades);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cidades editarCidade( @RequestBody Cidades cidades, @PathVariable Integer id ){
        return service.editar(cidades, id);
    }

}
