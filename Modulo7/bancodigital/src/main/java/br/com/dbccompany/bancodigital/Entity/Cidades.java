package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CIDADES" )
public class Cidades {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_CIDADES", sequenceName = "SEQ_CIDADES" )
    @GeneratedValue( generator = "SEQ_CIDADES", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID_CIDADE", nullable = false )
    private Integer idCidade;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable( name = "CLIENTES_X_CIDADES",
            joinColumns = { @JoinColumn ( name = "ID_CIDADE" ) },
            inverseJoinColumns = { @JoinColumn ( name = "FK_ID_CLIENTE" ) })
    private List<Clientes> clientes = new ArrayList<>();

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_ESTADO", nullable = false )
    private Estados idEstado;

    public Integer getIdCidade() {
        return idCidade;
    }
    public void setIdCidade(Integer idCidade) {
        this.idCidade = idCidade;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estados getIdEstado() {
        return idEstado;
    }
    public void setIdEstado(Estados idEstado) {
        this.idEstado = idEstado;
    }

    public List<Clientes> getClientes() {
        return clientes;
    }
    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }
}
