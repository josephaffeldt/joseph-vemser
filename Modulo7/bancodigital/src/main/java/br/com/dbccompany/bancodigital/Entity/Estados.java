package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "ESTADOS" )
public class Estados {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_ESTADOS", sequenceName = "SEQ_ESTADOS" )
    @GeneratedValue( generator = "SEQ_ESTADOS", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID_ESTADO", nullable = false )
    private Integer idEstado;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_PAIS", nullable = false)
    private Paises idPais;

    public Integer getIdEstado() {
        return idEstado;
    }
    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public Paises getIdPais() {
        return idPais;
    }
    public void setIdPais(Paises idPais) {
        this.idPais = idPais;
    }
}
