package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CONTAS" )
public class Contas {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_CONTAS", sequenceName = "SEQ_CONTAS" )
    @GeneratedValue( generator = "SEQ_CONTAS", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID_CONTA", nullable = false )
    private Integer idConta;

    @Column( name = "SALDO", nullable = false )
    private Double saldo;

    @Column( name = "NUMERO", nullable = false )
    private Integer numero;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_ID_TIPO", nullable = false )
    private Tipos idTipos;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "FK_CODIGO_AGENCIA", nullable = false )
    private Agencias codigoAgencia;

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable( name = "CLIENTES_X_CIDADES",
            joinColumns = { @JoinColumn ( name = "ID_CONTA" ) },
            inverseJoinColumns = { @JoinColumn ( name = "FK_ID_CLIENTE" ) })
    private List<Clientes> clientes = new ArrayList<>();

    public Integer getIdConta() {
        return idConta;    }

    public void setIdConta(Integer idConta) {
        this.idConta = idConta;
    }

    public Double getSaldo() {
        return saldo;
    }
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Integer getNumero() {
        return numero;
    }
    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Tipos getIdTipos() {
        return idTipos;
    }
    public void setIdTipos(Tipos idTipos) {
        this.idTipos = idTipos;
    }

    public Agencias getCodigoAgencia() {
        return codigoAgencia;
    }
    public void setCodigoAgencia(Agencias codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }
}
