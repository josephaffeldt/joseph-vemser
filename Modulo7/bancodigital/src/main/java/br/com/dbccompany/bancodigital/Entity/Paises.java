package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PAISES" )
public class Paises {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_PAISES", sequenceName = "SEQ_PAISES" )
    @GeneratedValue( generator = "SEQ_PAISES", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID_PAIS", nullable = false )
    private Integer idPais;

    @Column( name = "NOME", nullable = false )
    private String nome;

    public Integer getIdPais() {
        return idPais;
    }
    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
}
