package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Contas;
import br.com.dbccompany.bancodigital.Service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/conta" )
public class ContaController {

    @Autowired
    ContaService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Contas> todasContas(){
        return service.todasContas();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Contas buscarPorId(@PathVariable Integer id ) {
        return service.contaEspecifica(id);
    }

    @GetMapping(value = "/{numero}")
    @ResponseBody
    public Contas buscarPorNumero( @PathVariable Integer numero) {
        return service.contaPorNumero(numero);
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Contas novaConta( @RequestBody Contas conta ){
        return service.salvar(conta);
    }

    @PutMapping( value = "/editar" )
    @ResponseBody
    public Contas editarConta( @RequestBody Contas conta, @PathVariable Integer id ){
        return service.editar(conta, id);
    }
}
