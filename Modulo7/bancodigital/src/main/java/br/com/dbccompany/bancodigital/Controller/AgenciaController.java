package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Service.AgenciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/agencias" )
public class AgenciaController {

    @Autowired
    AgenciaService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Agencias> todasAgencias(){
        return service.todasAgencias();
    }

    @GetMapping( value = "/agencia/{id}" )
    @ResponseBody
    public Agencias agenciPorId( @RequestBody Agencias agencia, @PathVariable Integer id ){
        return service.editar(agencia, id);
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Agencias novaAgencia( @RequestBody Agencias agencia ){
        return service.salvar(agencia);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Agencias editarAgencia( @RequestBody Agencias agencia, @PathVariable Integer id ){
        return service.editar(agencia, id);
    }
}
