package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "TIPOS" )
public class Tipos {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_TIPOS", sequenceName = "SEQ_TIPOS" )
    @GeneratedValue( generator = "SEQ_TIPOS", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID_TIPO", nullable = false )
    private Integer idTipo;

    @Enumerated( EnumType.STRING )
    @Column( name = "DESCRICAO", nullable = false )
    private Descricao descricao;

    public Integer getIdTipo() {
        return idTipo;
    }
    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    public Descricao getDescricao() {
        return descricao;
    }
    public void setDescricao(Descricao descricao) {
        this.descricao = descricao;
    }
}
