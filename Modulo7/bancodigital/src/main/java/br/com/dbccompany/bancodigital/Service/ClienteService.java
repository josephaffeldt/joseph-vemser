package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService{

    @Autowired
    ClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Clientes salvar( Clientes cliente){
        return repository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public Clientes editar( Clientes cliente, Integer id ){
        cliente.setIdCliente(id);
        return repository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<Clientes> todosClientes(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public Clientes clienteEspecifico(Integer id ){
        Optional<Clientes> cliente = repository.findById(id);
        return cliente.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<Clientes> clientPorNome( String nome ){
        return repository.findAllByNome(nome);
    }

    @Transactional( rollbackFor = Exception.class )
    public Clientes clientePorCPF( Integer cpf ){
        return repository.findByCpf(cpf);
    }

}
