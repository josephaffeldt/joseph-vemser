package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientes" )
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Clientes> todosClientes(){
        return service.todosClientes();
    }

    @GetMapping( value = "/{nome}" )
    @ResponseBody
    public List<Clientes> buscarPorNome( @PathVariable String nome ){
        return service.clientPorNome(nome);
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Clientes buscarPoId( @PathVariable Integer id ){
        return service.clienteEspecifico( id );
    }

    @GetMapping( value = "/{cpf}" )
    @ResponseBody
    public Clientes buscarPoCpf( @PathVariable Integer cpf ){
        return service.clientePorCPF(cpf);
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Clientes novCliente(@RequestBody Clientes cliente ){
        return service.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Clientes editarCliente(@RequestBody Clientes cliente, @PathVariable Integer id ){
        return service.editar(cliente, id);
    }
}
