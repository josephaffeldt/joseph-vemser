package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "MOVIMENTACOES" )
public class Movimentacoes {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_MOVIMENTACOES", sequenceName = "SEQ_MOVIMENTACOES" )
    @GeneratedValue( generator = "SEQ_MOVIMENTACOES", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID_MOVIMENTACAO", nullable = false )
    private Integer idMovimentacao;

    @Column( name = "VALOR", nullable = false )
    private Double valor;

    @Enumerated( EnumType.STRING )
    @Column( name = "TIPO_DE_MOVIMENTACAO", nullable = false )
    private TiposMovimentacoes tipoDeMovimentacao;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CONTA", nullable = false )
    private Contas idConta;

    public Integer getIdMovimentacao() {
        return idMovimentacao;
    }
    public void setIdMovimentacao(Integer idMovimentacao) {
        this.idMovimentacao = idMovimentacao;
    }

    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TiposMovimentacoes getTipoDeMovimentacao() {
        return tipoDeMovimentacao;
    }
    public void setTipoDeMovimentacao(TiposMovimentacoes tipoDeMovimentacao) {
        this.tipoDeMovimentacao = tipoDeMovimentacao;
    }

    public Contas getIdConta() {
        return idConta;
    }
    public void setIdConta(Contas idConta) {
        this.idConta = idConta;
    }
}
