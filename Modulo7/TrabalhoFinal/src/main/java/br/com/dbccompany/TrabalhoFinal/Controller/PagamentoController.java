package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.PagamentoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoPagamento;
import br.com.dbccompany.TrabalhoFinal.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pagamentos" )
public class PagamentoController {

    @Autowired
    PagamentoService service;

    //Post
    @PostMapping( value = "/criar")
    @ResponseBody
    public PagamentoEntity criar( @RequestBody PagamentoEntity pagamento ) {
        return service.salvar(pagamento);
    }

    //Put
    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public PagamentoEntity editar( @RequestBody PagamentoEntity pagamentos, @PathVariable Integer id) {
        return service.editar(pagamentos, id);
    }

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PagamentoEntity> todosPagamentos() {
        return service.mostrarTodos();
    }

    @GetMapping( value = "/especifico/{id}" )
    @ResponseBody
    public PagamentoEntity buscarPorId( @PathVariable Integer id ) {
        return service.buscarPorId(id);
    }

    @GetMapping( value = "/tipoPagamento/{tipoPagamento}" )
    @ResponseBody
    public List<PagamentoEntity> buscarPorPagamento( @PathVariable TipoPagamento tipoPagamento ) {
        return service.buscarPorTipoPagamento( tipoPagamento );
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover ( @PathVariable Integer id ) {
        service.deletar(id);
    }
}

