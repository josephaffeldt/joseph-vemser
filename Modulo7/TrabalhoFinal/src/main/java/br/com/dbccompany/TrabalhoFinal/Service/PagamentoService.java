package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.PagamentoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoPagamento;
import br.com.dbccompany.TrabalhoFinal.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public PagamentoEntity salvar( PagamentoEntity pagamento ){
        return repository.save(pagamento);
    }

    @Transactional( rollbackFor = Exception.class )
    public PagamentoEntity editar( PagamentoEntity pagamento, Integer id ){
        pagamento.setId(id);
        return repository.save(pagamento);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<PagamentoEntity> mostrarTodos(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public PagamentoEntity buscarPorId( Integer id ){
        Optional<PagamentoEntity> pagamento = repository.findById(id);
        return pagamento.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<PagamentoEntity> buscarPorTipoPagamento( TipoPagamento tipoPagamento ){
        return repository.findAllByTipoPagamento(tipoPagamento);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }

}
