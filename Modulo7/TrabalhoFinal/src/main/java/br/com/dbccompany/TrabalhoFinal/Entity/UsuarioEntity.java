package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.security.MessageDigest;

@Entity
@Table( name = "USUARIO" )
public class UsuarioEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_USUARIO", sequenceName = "SEQ_USUARIO")
    @GeneratedValue( generator = "SEQ_USUARIO", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Email
    @Column( name = "EMAIL", nullable = false, unique = true )
    private String email;

    @Column( name = "LOGIN", nullable = false, unique = true )
    private String login;

    @Size( min = 6 )
    @Column( name = "SENHA", nullable = false )
    private String senha;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha){
        this.senha = this.senha;
    }

}
