package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PAGAMENTO" )
public class PagamentoEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_PAGAMENTO", sequenceName = "SEQ_PAGAMENTO" )
    @GeneratedValue( generator = "SEQ_PAGAMENTO", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_PAGAMENTO", nullable = false )
    private TipoPagamento tipoPagamento;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "CLIENTE_PACOTE", nullable = false )
    private ClientePacoteEntity clientePacote;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "ID_CONTRATACAO", nullable = false )
    private ContratacaoEntity contratacao;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }
    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientePacoteEntity getClientePacote() {
        return clientePacote;
    }
    public void setClientePacote(ClientePacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }
    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

}
