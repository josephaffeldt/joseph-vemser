package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteKey implements Serializable {

    @Column( name = "ID_CLIENTE" )
    private Long idCliente;

    @Column( name = "ID_ESPACO" )
    private Long idEspaco;

    public Long getIdCliente() {
        return idCliente;
    }
    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public Long getIdEspaco() {
        return idEspaco;
    }
    public void setIdEspaco(Long idEspaco) {
        this.idEspaco = idEspaco;
    }
}
