package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.EspacoPacoteEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<EspacoPacoteEntity, Integer> {

    List<EspacoPacoteEntity> findAllByTipoContratacao( TipoContratacao tipoContratacao );
    List<EspacoPacoteEntity> findAll();
}
