package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.PacoteEntity;
import br.com.dbccompany.TrabalhoFinal.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pacotes" )
public class PacoteController {

    @Autowired
    PacoteService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public PacoteEntity criarPacote( @RequestBody PacoteEntity pacote ){
        return service.salvar(pacote);
    }

    //Put
    @PostMapping( value = "/editar/{id}" )
    @ResponseBody
    public PacoteEntity editarPacote(@RequestBody PacoteEntity pacote, @PathVariable Integer id ){
        return service.editar(pacote, id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PacoteEntity> todosPacotes(){
        return service.mostrarTodos();
    }

    @GetMapping( value = "/especifico/{id}" )
    @ResponseBody
    public PacoteEntity buscarPorId( @PathVariable Integer id ){
        return service.buscaPorId(id);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( @PathVariable Integer id ){
        service.deletar(id);
    }
}
