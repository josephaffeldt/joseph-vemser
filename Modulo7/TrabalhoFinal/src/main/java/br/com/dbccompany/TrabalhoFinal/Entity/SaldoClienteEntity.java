package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "SALDO_CLIENTE" )
public class SaldoClienteEntity {

    @EmbeddedId
    private SaldoClienteKey id;

    @ManyToOne( cascade = CascadeType.MERGE)
    @MapsId( "ID_CLIENTE" )
    @JoinColumn( name = "ID_CLIENTE", nullable = false)
    private ClienteEntity cliente;

    @ManyToOne( cascade = CascadeType.MERGE)
    @MapsId( "ID_ESPACO" )
    @JoinColumn( name = "ID_ESPACO", nullable = false)
    private EspacoEntity espaco;

    @Enumerated( EnumType.STRING )
    @Column( name = "TIPO_CONTRATACAO", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @Column( name = "VENCIMENTO", nullable = false )
    private Date vencimento;

    public SaldoClienteKey getId() {
        return id;
    }
    public void setId(SaldoClienteKey id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }
    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }
    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }
    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }
    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

}
