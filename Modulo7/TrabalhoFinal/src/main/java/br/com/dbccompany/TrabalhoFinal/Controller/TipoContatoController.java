package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.TipoContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "api/tipoContato" )
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public TipoContatoEntity criarTipoContato( @RequestBody TipoContatoEntity tipoContato ){
        return service.salvar(tipoContato);
    }

    //Put
    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContatoEntity editarTipoContato( @RequestBody TipoContatoEntity tipoContato, @PathVariable Integer id){
        return service.editar(tipoContato, id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TipoContatoEntity> todosTiposContatos(){
        return service.mostrarTodos();
    }

    @GetMapping( value = "/idEspecifico/{id}" )
    @ResponseBody
    public TipoContatoEntity buscarPorId( @PathVariable Integer id ){
        return service.buscarPorId(id);
    }

    @GetMapping( value = "/nomeEspecifico/{tipo}" )
    @ResponseBody
    public List<TipoContatoEntity> buscarPorNome( @PathVariable String nome ){
        return service.buscarPorNome(nome);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( @PathVariable Integer id ){
        service.deletar(id);
    }
}