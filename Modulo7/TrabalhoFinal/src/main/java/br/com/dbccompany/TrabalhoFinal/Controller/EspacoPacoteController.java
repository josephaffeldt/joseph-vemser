package br.com.dbccompany.TrabalhoFinal.Controller;


import br.com.dbccompany.TrabalhoFinal.Entity.EspacoPacoteEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import br.com.dbccompany.TrabalhoFinal.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacosPacotes" )
public class EspacoPacoteController {

    @Autowired
    EspacoPacoteService service;

    //Post
    @PostMapping( "/criar" )
    @ResponseBody
    public EspacoPacoteEntity criar( @RequestBody EspacoPacoteEntity espacoPacote ){
        return service.salvar(espacoPacote);
    }

    //Put
    @PutMapping( "/editar/{id}" )
    @ResponseBody
    public EspacoPacoteEntity editar( @RequestBody EspacoPacoteEntity espacoPacote, @PathVariable Integer id ){
        return service.editar(espacoPacote,id);
    }

    //Get
    @GetMapping( "/todos" )
    @ResponseBody
    public List<EspacoPacoteEntity> todosEspacos(){
        return service.mostrarTodos();
    }

    @GetMapping( "/tipoEspecifico/{tipo}" )
    @ResponseBody
    public List<EspacoPacoteEntity> buscarPorTipoContratacao( TipoContratacao tipoContratacao ){
        return service.buscarPorTipoContratacao(tipoContratacao);
    }

    @GetMapping( "/idEspecifico/{id}" )
    @ResponseBody
    public EspacoPacoteEntity buscarPorId( Integer id ){
        return service.espacosPacotesEspecifico(id);
    }

    //Delete
    @DeleteMapping( "/remover/{id}" )
    @ResponseBody
    public void remover( Integer id ){
        service.deletar(id);
    }

}
