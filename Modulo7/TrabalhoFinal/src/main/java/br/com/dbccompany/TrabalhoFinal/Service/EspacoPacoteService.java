package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.EspacoPacoteEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import br.com.dbccompany.TrabalhoFinal.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoPacoteService {

    @Autowired
    EspacoPacoteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public EspacoPacoteEntity salvar(EspacoPacoteEntity espacoPacote ) {
        return repository.save(espacoPacote);
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoPacoteEntity editar(EspacoPacoteEntity espacoPacote, Integer id ) {
        espacoPacote.setId(id);
        return repository.save(espacoPacote);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<EspacoPacoteEntity> mostrarTodos() {
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<EspacoPacoteEntity> buscarPorTipoContratacao(TipoContratacao tipoContratacao) {
        return repository.findAllByTipoContratacao(tipoContratacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoPacoteEntity espacosPacotesEspecifico( Integer id ) {
        Optional<EspacoPacoteEntity> espacoPacote = repository.findById(id);
        return espacoPacote.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }
}
