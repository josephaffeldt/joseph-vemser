package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTRATACAO" )
public class ContratacaoEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_CONTRATACAO", sequenceName = "SEQ_CONTRATACAO" )
    @GeneratedValue( generator = "SEQ_CONTRATACAO", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_CONTRATACAO", nullable = false )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @Column( name = "DESCONTO")
    private Double desconto;

    @Column( name = "PRAZO", nullable = false )
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_ESPACO", nullable = false)
    private EspacoEntity espaco;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTE", nullable = false)
    private ClienteEntity cliente;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }
    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }
    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }
    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }
    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }
    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

}
