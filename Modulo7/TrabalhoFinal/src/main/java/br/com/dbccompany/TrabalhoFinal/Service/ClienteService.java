package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.ClienteEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.ContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import br.com.dbccompany.TrabalhoFinal.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity salvar( ClienteEntity cliente ){
        TipoContatoEntity telefone = new TipoContatoEntity();
        telefone.setNome("TELEFONE");
        TipoContatoEntity email = new TipoContatoEntity();
        email.setNome("EMAIL");

        ContatoEntity contatoTel = new ContatoEntity();
        contatoTel.setTipo(telefone);
        ContatoEntity contatoEmail = new ContatoEntity();
        contatoEmail.setTipo(email);

        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contatoTel);
        contatos.add(contatoEmail);

        cliente.setContato(contatos);
        return repository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity editar( ClienteEntity cliente, Integer id ){
        cliente.setId(id);
        return repository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<ClienteEntity> mostrarTodos(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity buscarPorId( Integer id ){
        Optional<ClienteEntity> cliente = repository.findById(id);
        return cliente.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public ClienteEntity buscarPorCpf( Integer cpf ){
        return repository.findByCpf(cpf);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<ClienteEntity> buscarPorNome( String nome ){
        return repository.findAllByNome(nome);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<ClienteEntity> buscarPorDataNascimento( Date data ){
        return repository.findAllByDataNascimento(data);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }

}
