package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.SaldoClienteEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.SaldoClienteKey;
import br.com.dbccompany.TrabalhoFinal.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    SaldoClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public SaldoClienteEntity salvar( SaldoClienteEntity saldoCliente ) {
        return repository.save( saldoCliente );
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoClienteEntity editar(SaldoClienteEntity saldoCliente, SaldoClienteKey id ) {
        saldoCliente.setId( id );
        return repository.save( saldoCliente );
    }

    @Transactional( rollbackFor = Exception.class)
    public List<SaldoClienteEntity> mostrarTodos() {
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoClienteEntity buscarPorId( SaldoClienteKey id ) {
        Optional<SaldoClienteEntity> saldoCliente = repository.findById(id);
        return saldoCliente.get();
    }

    @Transactional( rollbackFor = Exception.class)
    public void deletar( SaldoClienteKey id ){
        repository.deleteById(id);
    }
}
