package br.com.dbccompany.TrabalhoFinal.Entity;

public enum TipoPagamento {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA;
}
