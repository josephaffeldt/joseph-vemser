package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.AcessoEntity;
import br.com.dbccompany.TrabalhoFinal.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/acessos" )
public class AcessoController {

    @Autowired
    AcessoService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public AcessoEntity criar( @RequestBody AcessoEntity acesso ) {
        return service.salvar(acesso);
    }

    //Put
    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public AcessoEntity editar( @RequestBody AcessoEntity acesso, @PathVariable Integer id ) {
        return service.editar( acesso, id );
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<AcessoEntity> todosoAcessos() {
        return service.mostrarTodos();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public AcessoEntity buscarPorId( @PathVariable  Integer id ) {
        return service.buscarPorId( id );
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( Integer id ){
        service.deletar(id);
    }

}
