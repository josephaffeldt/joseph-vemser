package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.ContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ContatoEntity salvar( ContatoEntity contato ){
        return repository.save(contato);
    }

    @Transactional( rollbackFor = Exception.class )
    public ContatoEntity editar( ContatoEntity contato, Integer id ){
        contato.setId(id);
        return repository.save(contato);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<ContatoEntity> mostrarTodos(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public ContatoEntity buscarPorId( Integer id ){
        Optional<ContatoEntity> contato = repository.findById(id);
        return contato.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<ContatoEntity> buscarPorTipo( TipoContatoEntity tipo ){
        return repository.findAllByTipo(tipo);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }
}
