package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.PacoteEntity;
import br.com.dbccompany.TrabalhoFinal.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacoteService {

    @Autowired
    PacoteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public PacoteEntity salvar( PacoteEntity pacote ){
        return repository.save(pacote);
    }

    @Transactional( rollbackFor = Exception.class )
    public PacoteEntity editar( PacoteEntity pacote, Integer id ){
        pacote.setId(id);
        return repository.save(pacote);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<PacoteEntity> mostrarTodos(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public PacoteEntity buscaPorId( Integer id ){
        Optional<PacoteEntity> pacote = repository.findById(id);
        return pacote.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }
}
