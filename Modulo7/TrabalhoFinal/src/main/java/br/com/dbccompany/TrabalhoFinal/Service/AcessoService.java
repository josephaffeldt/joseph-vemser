package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.AcessoEntity;
import br.com.dbccompany.TrabalhoFinal.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    AcessoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public AcessoEntity salvar(AcessoEntity acesso ) {
        if(acesso.getData() == null){
            acesso.setData(new Date());
        }
        return repository.save( acesso );
    }

    @Transactional( rollbackFor = Exception.class)
    public AcessoEntity editar( AcessoEntity acesso, Integer id ) {
        acesso.setId( id );
        return repository.save( acesso );
    }

    @Transactional( rollbackFor = Exception.class )
    public List<AcessoEntity> mostrarTodos() {
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public AcessoEntity buscarPorId( Integer id ) {
        Optional<AcessoEntity> acesso = repository.findById( id );
        return acesso.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }

}
