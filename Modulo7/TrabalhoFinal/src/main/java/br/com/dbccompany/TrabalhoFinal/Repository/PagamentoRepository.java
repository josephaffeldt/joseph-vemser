package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.PagamentoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {

    List<PagamentoEntity> findAllByTipoPagamento( TipoPagamento tipoPagamento );
    List<PagamentoEntity> findAll();

}
