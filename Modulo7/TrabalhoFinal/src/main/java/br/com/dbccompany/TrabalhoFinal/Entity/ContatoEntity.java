package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTATO" )
public class ContatoEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_CONTATO", sequenceName = "SEQ_CONTATO" )
    @GeneratedValue( generator = "SEQ_CONTATO", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "VALOR", nullable = false)
    private Double valor;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "TIPO_CONTATO", nullable = false )
    private TipoContatoEntity tipo;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "ID_CLIENTE", nullable = false )
    private ClienteEntity cliente;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TipoContatoEntity getTipo() {
        return tipo;
    }
    public void setTipo(TipoContatoEntity tipo) {
        this.tipo = tipo;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }
    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
