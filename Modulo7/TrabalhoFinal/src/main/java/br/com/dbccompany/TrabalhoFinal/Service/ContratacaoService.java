package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.EspacoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import br.com.dbccompany.TrabalhoFinal.Repository.ContratacaoRepository;
import br.com.dbccompany.TrabalhoFinal.Repository.EspacoRepository;
import org.graalvm.compiler.nodes.calc.IntegerDivRemNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    ContratacaoRepository repository;

    @Autowired
    EspacoRepository eRepository;

    @Transactional( rollbackFor = Exception.class )
    public ContratacaoEntity salvar( ContratacaoEntity contratacao ) {
        return repository.save(contratacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public ContratacaoEntity editar( ContratacaoEntity contratacao, Integer id ) {
        contratacao.setId(id);
        return repository.save(contratacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<ContratacaoEntity> mostrarTodos() {
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public ContratacaoEntity buscarPorId ( Integer id ) {
        Optional<ContratacaoEntity> contratacao = repository.findById(id);
        return contratacao.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<ContratacaoEntity> buscarPorTipo( TipoContratacao contratacao ) {
        return repository.findAllByTipoContratacao(contratacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById(id);
    }

    @Transactional( rollbackFor = Exception.class )
    public Double calcularContrato( Integer idContratacao, Integer idEspaco ){
        Optional<ContratacaoEntity> contratacao = repository.findById(idContratacao);
        Optional<EspacoEntity> espaco = eRepository.findById(idEspaco);
        Double valor = contratacao.get().getQuantidade() * espaco.get().getValor();

        if ( contratacao.get().getDesconto() > 0 && contratacao.get().getDesconto() < 100 ){
            return valor - ( contratacao.get().getDesconto() * valor );
        }

        return valor;
    }


}
