package br.com.dbccompany.TrabalhoFinal.Service;

import org.apache.commons.lang3.StringUtils;
import br.com.dbccompany.TrabalhoFinal.Entity.UsuarioEntity;
import br.com.dbccompany.TrabalhoFinal.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository repository;

    public String passwordEncrypted( String senha ){
        String psEncrypted = "";

        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
            psEncrypted = hash.toString(16);
        }catch (Exception e ) { }

        return psEncrypted;
    }

    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity salvar( UsuarioEntity usuario ){
        String senha = usuario.getSenha();
        if( (StringUtils.isAlphanumeric(senha))){
            usuario.setSenha(passwordEncrypted(senha));
            return repository.save(usuario);
        }else {
            throw new RuntimeException("Obrigátorio senha de 6 digitos alfanumericos.");
        }
    }

    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity editar( UsuarioEntity usuario, Integer id ){
        usuario.setId(id);
        return repository.save(usuario);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<UsuarioEntity> mostrarTodos(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity buscarPorId( Integer id ){
        Optional<UsuarioEntity> usuario = repository.findById(id);
        return usuario.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<UsuarioEntity> buscarPorNome( String nome ){
        return repository.findAllByNome(nome);
    }

    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity buscarPorEmail( String email ){
        return repository.findAllByEmail(email);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }
}
