package br.com.dbccompany.TrabalhoFinal.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table( name = "CLIENTE" )
public class ClienteEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_CLIENTE", sequenceName = "SEQ_CLIENTE" )
    @GeneratedValue( generator = "SEQ_CLIENTE", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @CPF
    @Column( name = "CPF", nullable = false, unique = true )
    private Integer cpf;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy" )
    @Column( name = "DATA_NASCIMENTO", nullable = false )
    private Date dataNascimento;

    @OneToMany( mappedBy = "cliente" )
    private List<ContatoEntity> contato = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" )
    private List<ContratacaoEntity> contratacao = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" )
    private List<ClientePacoteEntity> clientePacote = new ArrayList<>();

    @OneToMany( mappedBy = "cliente")
    private List<SaldoClienteEntity> saldoCliente = new ArrayList<>();

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getCpf() {
        return cpf;
    }
    public void setCpf(Integer cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }
    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }
    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public List<ClientePacoteEntity> getClientePacote() {
        return clientePacote;
    }
    public void setClientePacote(List<ClientePacoteEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }
    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

}
