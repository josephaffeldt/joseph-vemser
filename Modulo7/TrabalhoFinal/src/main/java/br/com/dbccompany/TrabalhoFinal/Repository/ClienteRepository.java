package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {

    ClienteEntity findByCpf( Integer cpf );
    List<ClienteEntity> findAllByNome( String nome );
    List<ClienteEntity> findAllByDataNascimento(Date data);
    List<ClienteEntity> findAll();

}
