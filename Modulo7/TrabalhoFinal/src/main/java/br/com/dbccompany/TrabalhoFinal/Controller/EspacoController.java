package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.EspacoEntity;
import br.com.dbccompany.TrabalhoFinal.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacos" )
public class EspacoController {

    @Autowired
    EspacoService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public EspacoEntity criarEspaco ( @RequestBody EspacoEntity espaco ){
        return service.salvar(espaco);
    }

    //Put
    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacoEntity editar( @RequestBody EspacoEntity espaco, @PathVariable Integer id ){
        return service.editar(espaco, id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EspacoEntity> todosEspacos(){
        return service.mostrarTodos();
    }

    @GetMapping( value = "/idEspecifico/{id}" )
    @ResponseBody
    public EspacoEntity buscarPorId( Integer id ){
        return service.buscarPorId(id);
    }

    @GetMapping( value = "/nomeEspecifico/{nome}" )
    @ResponseBody
    public List<EspacoEntity> buscarPorNome( String nome ){
        return service.buscarPorNome(nome);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( Integer id ){
        service.deletar(id);
    }
}
