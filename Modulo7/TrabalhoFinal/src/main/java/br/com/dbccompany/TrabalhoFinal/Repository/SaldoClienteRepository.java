package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.SaldoClienteEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.SaldoClienteKey;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, Integer> {

    List<SaldoClienteEntity> findAll();
    List<SaldoClienteEntity> findAllByTipoContratacao( TipoContratacao contratacao );
    List<SaldoClienteEntity> findAllByVencimento( Date data );
    Optional<SaldoClienteEntity> findById(SaldoClienteKey id);
    void deleteById(SaldoClienteKey id);

}
