package br.com.dbccompany.TrabalhoFinal.Entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "ACESSOS" )
public class AcessoEntity {

    @SequenceGenerator( allocationSize = 1, name = "SEQ_ACESSO", sequenceName = "SEQ_ACESSO")
    @GeneratedValue( generator = "SEQ_ACESSO", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumns({
        @JoinColumn(
            name = "ID_CLIENTES_SALDO_CLIENTE",
            referencedColumnName = "ID_CLIENTE",
            nullable = false
        ),
        @JoinColumn(
            name = "ID_ESPACO_SALDO_CLIENTE",
            referencedColumnName = "ID_ESPACO",
            nullable = false
        )
    })
    private SaldoClienteEntity saldoCliente;

    @Column( name = "IS_ENTRADA", nullable = false )
    private Boolean isEntrada;

    @Column( name = "DATA", nullable = false )
    private Date data;

    @Column( name = "IS_EXCECAO", nullable = false )
    private Boolean isExcecao;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }
    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }
    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }
    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }
    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }

}
