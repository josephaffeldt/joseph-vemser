package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "PACOTE" )
public class PacoteEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_PACOTE", sequenceName = "SEQ_PACOTE" )
    @GeneratedValue( generator = "SEQ_PACOTE", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "VALOR", nullable = false )
    private Double valor;

    @OneToMany( mappedBy = "pacote" )
    private Set<EspacoEntity> espaco;

    @OneToMany( mappedBy = "pacote")
    private Set<ClienteEntity> cliente;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Set<EspacoEntity> getEspaco() {
        return espaco;
    }
    public void setEspaco(Set<EspacoEntity> espaco) {
        this.espaco = espaco;
    }

    public Set<ClienteEntity> getCliente() {
        return cliente;
    }
    public void setCliente(Set<ClienteEntity> cliente) {
        this.cliente = cliente;
    }

}
