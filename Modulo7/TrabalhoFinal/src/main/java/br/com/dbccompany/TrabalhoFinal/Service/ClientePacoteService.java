package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.ClientePacoteEntity;
import br.com.dbccompany.TrabalhoFinal.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientePacoteService {

    @Autowired
    ClientePacoteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ClientePacoteEntity salvar(ClientePacoteEntity clientePacote ) {
        return repository.save(clientePacote);
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientePacoteEntity editar( ClientePacoteEntity clientePacote, Integer id ) {
        clientePacote.setId(id);
        return repository.save(clientePacote);
    }

    public List<ClientePacoteEntity> mostrarTodos() {
        return repository.findAll();
    }

    public ClientePacoteEntity buscarPorId( Integer id ) {
        Optional<ClientePacoteEntity> clientePacote = repository.findById(id);
        return clientePacote.get();
    }

    public void deletar( Integer id ){
        repository.deleteById(id);
    }
}
