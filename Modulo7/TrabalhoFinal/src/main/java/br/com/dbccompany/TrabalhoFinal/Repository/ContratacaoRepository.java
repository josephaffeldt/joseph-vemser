package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {

    List<ContratacaoEntity> findAllByTipoContratacao(TipoContratacao tipo);
    List<ContratacaoEntity> findAll();

}
