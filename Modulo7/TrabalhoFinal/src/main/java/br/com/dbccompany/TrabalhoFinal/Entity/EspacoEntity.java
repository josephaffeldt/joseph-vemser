package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "ESPACO" )
public class EspacoEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_ESPACO", sequenceName = "SEQ_ESPACO" )
    @GeneratedValue( generator = "SEQ_ESPACO", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "NOME", nullable = false, unique = true)
    private String nome;

    @Column( name = "QTD_PESSOAS", nullable = false )
    private Integer qtdPessoas;

    @Column( name = "VALOR", nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacoteEntity> espacoPacote = new ArrayList<>();

    @OneToMany(mappedBy = "espaco" )
    private List<ContratacaoEntity> contratacao = new ArrayList<>();

    @OneToMany( mappedBy = "espaco")
    private List<SaldoClienteEntity> saldoCliente = new ArrayList<>();

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }
    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacoPacoteEntity> getEspacoPacote() {
        return espacoPacote;
    }
    public void setEspacoPacote(List<EspacoPacoteEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }
    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }
    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

}
