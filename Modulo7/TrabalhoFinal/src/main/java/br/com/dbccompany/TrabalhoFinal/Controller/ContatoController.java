package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.ContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "api/contatos" )
public class ContatoController {

    @Autowired
    ContatoService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public ContatoEntity criarContato( @RequestBody ContatoEntity contato ){
        return service.salvar(contato);
    }

    //Put
    @PutMapping( value = "editar/{id}" )
    @ResponseBody
    public ContatoEntity editarContato( @RequestBody ContatoEntity contato, @PathVariable Integer id ){
        return service.editar(contato, id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContatoEntity> todosContatos(){
        return service.mostrarTodos();
    }

    @GetMapping( value = "/especifico/{id}" )
    @ResponseBody
    public ContatoEntity buscarPorId( @PathVariable Integer id ){
        return service.buscarPorId(id);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( @PathVariable Integer id ){
        service.deletar( id );
    }
}
