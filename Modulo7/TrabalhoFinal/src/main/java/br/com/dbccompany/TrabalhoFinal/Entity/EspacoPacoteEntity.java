package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@Table( name = "ESPACO_PACOTE" )
public class EspacoPacoteEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_ESPACO_PACOTE", sequenceName = "SEQ_ESPACO_PACOTE" )
    @GeneratedValue( generator = "SEQ_ESPACO_PACOTE", strategy = GenerationType.SEQUENCE )
    @Column( name = "ID", nullable = false )
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_CONTRATACAO", nullable = false )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @Column( name = "PRAZO", nullable = false )
    private Integer prazo;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "ID_ESPACO", nullable = false)
    private EspacoEntity espaco;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "ID_PACOTE", nullable = false)
    private PacoteEntity pacote;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }
    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }
    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }
    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }
    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

}
