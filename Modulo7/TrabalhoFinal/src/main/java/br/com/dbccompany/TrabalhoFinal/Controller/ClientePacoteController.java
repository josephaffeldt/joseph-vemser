package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.ClientePacoteEntity;
import br.com.dbccompany.TrabalhoFinal.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientesPacotes" )
public class ClientePacoteController {

    @Autowired
    ClientePacoteService service;

    //Post
    @PostMapping( value = "/criar")
    @ResponseBody
    public ClientePacoteEntity novoClientesPacotes( @RequestBody ClientePacoteEntity clientePacote) {
        return service.salvar(clientePacote);
    }

    //Put
    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientePacoteEntity editarClientesPacotes(@RequestBody ClientePacoteEntity clientePacote, @PathVariable Integer id ){
        return service.editar(clientePacote, id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientePacoteEntity> todosClientesPacotes() {
        return service.mostrarTodos();
    }

    @GetMapping( value = "/especifico/{id}" )
    @ResponseBody
    public ClientePacoteEntity buscarPorId ( @PathVariable Integer id ) {
        return service.buscarPorId(id);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( @PathVariable Integer id ) {
        service.deletar(id);
    }
    }
