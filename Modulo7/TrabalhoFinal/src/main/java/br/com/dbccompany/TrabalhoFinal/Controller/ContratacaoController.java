package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.EspacoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContratacao;
import br.com.dbccompany.TrabalhoFinal.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contratos" )
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    //Post
    @PostMapping( "/criar" )
    @ResponseBody
    public ContratacaoEntity criar( @RequestBody ContratacaoEntity contratacao ){
        return service.salvar(contratacao);
    }

    //Put
    @PutMapping( "/editar/{id}" )
    @ResponseBody
    public ContratacaoEntity criar( @RequestBody ContratacaoEntity contratacao, @PathVariable Integer id ){
        return service.editar(contratacao,id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContratacaoEntity> todosEspacos(){
        return service.mostrarTodos();
    }

    @GetMapping( value = "/idEspecifico/{id}" )
    @ResponseBody
    public ContratacaoEntity buscarPorId( Integer id ){
        return service.buscarPorId(id);
    }

    @GetMapping( value = "/tipoEspecifico/{tipo}" )
    @ResponseBody
    public List<ContratacaoEntity> buscarPorTipo( TipoContratacao tipo ){
        return service.buscarPorTipo(tipo);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( Integer id ){
        service.deletar(id);
    }
}
