package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {

    List<EspacoEntity> findAllByNome(String nome);
    List<EspacoEntity> findAll();

}
