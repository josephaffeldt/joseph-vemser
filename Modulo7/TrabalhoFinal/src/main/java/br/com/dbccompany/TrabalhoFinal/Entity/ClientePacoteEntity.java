package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CLIENTE_PACOTE" )
public class ClientePacoteEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_CLIENTE_PACOTE", sequenceName = "SEQ_CLIENTE_PACOTE" )
    @GeneratedValue( generator = "SEQ_CLIENTE_PACOTE", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "ID_CLIENTE", nullable = false)
    private ClienteEntity cliente;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "ID_PACOTE", nullable = false   )
    private PacoteEntity pacote;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }
    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }
    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

}
