package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.EspacoEntity;
import br.com.dbccompany.TrabalhoFinal.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {

    @Autowired
    EspacoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public EspacoEntity salvar( EspacoEntity espaco ){
        return repository.save(espaco);
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoEntity editar( EspacoEntity espaco, Integer id ){
        espaco.setId(id);
        return repository.save(espaco);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<EspacoEntity> mostrarTodos (){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacoEntity buscarPorId( Integer id ){
        Optional<EspacoEntity> espaco = repository.findById(id);
        return espaco.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<EspacoEntity> buscarPorNome( String nome ){
        return repository.findAllByNome(nome);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }


}
