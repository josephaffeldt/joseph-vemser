package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.UsuarioEntity;
import br.com.dbccompany.TrabalhoFinal.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/usuarios" )
public class UsuarioController {

    @Autowired
    UsuarioService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public UsuarioEntity criar( @RequestBody UsuarioEntity usuario ){
        return service.salvar(usuario);
    }

    //Put
    @PutMapping( value = "/editar" )
    @ResponseBody
    public UsuarioEntity editar( @RequestBody UsuarioEntity usuario, @PathVariable Integer id ){
        return service.editar(usuario,id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<UsuarioEntity> todosUsuarios(){
        return service.mostrarTodos();
    }

    @GetMapping( value = "/idEspecifico/{id}" )
    @ResponseBody
    public UsuarioEntity buscarPorId( @PathVariable Integer id ){
        return service.buscarPorId(id);
    }

    @GetMapping( value = "/nomeEspecifico/{nome}")
    @ResponseBody
    public List<UsuarioEntity> buscarPorNome( @PathVariable String nome ){
        return service.buscarPorNome(nome);
    }

    @GetMapping( value = "/emailEspecifico/{email}")
    @ResponseBody
    public UsuarioEntity buscarPorEmail( @PathVariable String email ){
        return service.buscarPorEmail(email);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}")
    @ResponseBody
    public void remover( @PathVariable Integer id ){
        service.deletar(id);
    }
}
