package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.ClienteEntity;
import br.com.dbccompany.TrabalhoFinal.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping( "api/clientes" )
public class ClienteController {

    @Autowired
    ClienteService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public ClienteEntity criarCliente( @RequestBody ClienteEntity cliente ){
        return service.salvar(cliente);
    }

    //Put
    @PutMapping( value = "editar/{id}" )
    @ResponseBody
    public ClienteEntity editarCliente(@RequestBody ClienteEntity cliente, @PathVariable Integer id ){
        return service.editar(cliente, id);
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClienteEntity> todosClientes(){
        return service.mostrarTodos();
    }

    @GetMapping( value = "/idEspecifico/{id}" )
    @ResponseBody
    public ClienteEntity buscarPorId( @PathVariable Integer id ){
        return service.buscarPorId(id);
    }

    @GetMapping( value = "/cpfEspecifico/{cpf}" )
    @ResponseBody
    public ClienteEntity buscarPorCpf( @PathVariable Integer cpf ){
        return service.buscarPorCpf(cpf);
    }

    @GetMapping( value = "/nomeEspecifico/{nome}" )
    @ResponseBody
    public List<ClienteEntity> buscarPorNome( @PathVariable String nome ){
        return service.buscarPorNome(nome);
    }

    @GetMapping( value = "/dataEspecifica/{data}")
    @ResponseBody
    public List<ClienteEntity> buscarPorData( @PathVariable Date data ){
        return service.buscarPorDataNascimento(data);
    }

    //Deletar
    @DeleteMapping( value = "/remover/{id}")
    @ResponseBody
    public void remover( @PathVariable Integer id ){
        service.deletar(id);
    }
}
