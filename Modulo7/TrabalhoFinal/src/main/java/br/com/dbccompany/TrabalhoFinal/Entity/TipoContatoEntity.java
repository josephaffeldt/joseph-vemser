package br.com.dbccompany.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "TIPO_CONTATO" )
public class TipoContatoEntity {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "SEQ_TIPO_CONTATO", sequenceName = "SEQ_TIPO_CONTATO" )
    @GeneratedValue( generator = "SEQ_TIPO_CONTATO", strategy = GenerationType.SEQUENCE )

    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @OneToMany( mappedBy = "tipoContato" )
    private List<ContatoEntity> contato = new ArrayList<>();

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }
    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }

}
