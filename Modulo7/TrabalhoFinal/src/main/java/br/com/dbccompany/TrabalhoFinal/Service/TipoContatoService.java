package br.com.dbccompany.TrabalhoFinal.Service;

import br.com.dbccompany.TrabalhoFinal.Entity.TipoContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    TipoContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public TipoContatoEntity salvar( TipoContatoEntity tipoContato ){
        return repository.save(tipoContato);
    }

    @Transactional( rollbackFor = Exception.class )
    public TipoContatoEntity editar( TipoContatoEntity tipoContato, Integer id ){
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }

    @Transactional( rollbackFor = Exception.class )
    public List<TipoContatoEntity> mostrarTodos(){
        return repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public TipoContatoEntity buscarPorId( Integer id ){
        Optional<TipoContatoEntity> tipoContato = repository.findById(id);
        return tipoContato.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public List<TipoContatoEntity> buscarPorNome( String nome ){
        return repository.findAllByNome( nome );
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ){
        repository.deleteById(id);
    }
}
