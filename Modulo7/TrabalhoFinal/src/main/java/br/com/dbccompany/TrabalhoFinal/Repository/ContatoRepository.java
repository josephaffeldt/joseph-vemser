package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.ContatoEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatoRepository extends CrudRepository <ContatoEntity, Integer> {

    List<ContatoEntity> findAllByTipo(TipoContatoEntity tipo);
    List<ContatoEntity> findAll();
}
