package br.com.dbccompany.TrabalhoFinal.Entity;

public enum TipoContratacao {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES;
}
