package br.com.dbccompany.TrabalhoFinal.Controller;

import br.com.dbccompany.TrabalhoFinal.Entity.SaldoClienteEntity;
import br.com.dbccompany.TrabalhoFinal.Entity.SaldoClienteKey;
import br.com.dbccompany.TrabalhoFinal.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/saldoClientes" )
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    //Post
    @PostMapping( value = "/criar" )
    @ResponseBody
    public SaldoClienteEntity criar( @RequestBody SaldoClienteEntity saldoCliente ) {
        return service.salvar( saldoCliente );
    }

    //Put
    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldoClienteEntity editar( @RequestBody SaldoClienteEntity saldoCliente, @PathVariable SaldoClienteKey id ) {
        return service.editar( saldoCliente, id );
    }

    //Get
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<SaldoClienteEntity> todosSaldoCliente() {
        return service.mostrarTodos();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public SaldoClienteEntity buscarPorId( @PathVariable  SaldoClienteKey id ) {
        return service.buscarPorId(id);
    }

    //Delete
    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public void remover( SaldoClienteKey id ){
        service.deletar(id);
    }

}
