package br.com.dbccompany.TrabalhoFinal.Repository;

import br.com.dbccompany.TrabalhoFinal.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

    List<UsuarioEntity> findAllByNome(String nome);
    UsuarioEntity findAllByEmail(String email);
    List<UsuarioEntity> findAll();

}
